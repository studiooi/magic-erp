package com.hys.app.framework.apptoken;

/**
 * access token管理接接口
 * @author kingapex
 * @data 2021/8/22 16:26
 * @version 1.0
 **/
public interface AccessTokenManager {

    /**
     *  获取缓存的token，如果过期会重新刷新token、缓存并返回
     * @param accessTokenIdentifier token的唯一标识
     *
     * @return
     */
    String  getToken(AccessTokenIdentifier accessTokenIdentifier);


    /**
     * 强制刷新token
     * 会调用api重新获取token,刷新token、缓存并返回
     * @param accessTokenIdentifier token的唯一标识
     */
    String refreshToken(AccessTokenIdentifier accessTokenIdentifier);
}
