package com.hys.app.framework.auth.impl;

import com.hys.app.framework.auth.TokenParseException;
import com.hys.app.framework.auth.TokenParser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

/**
 * jwt token解析器
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-06-24
 */

public class JwtTokenParser implements TokenParser {

    private String secret;

    private Claims claims;

    public JwtTokenParser(String secret) {
        this.secret = secret;
    }


    @Override
    public <T> T parse(Class<T> clz, String token) throws TokenParseException {

        try {
            claims
                    = Jwts.parser()
                    .setSigningKey(secret.getBytes())
                    .parseClaimsJws(token).getBody();

            Object obj = claims.get("uid");
            claims.put("uid", Long.valueOf(obj.toString()));

            Object platformShopId = claims.get("platformShopId");
            if (Objects.nonNull(platformShopId)) {
                claims.put("platformShopId", Long.valueOf(platformShopId.toString()));
            }

            Object clerkId = claims.get("clerkId");
            if(clerkId != null){
                claims.put("clerkId", Long.parseLong(clerkId.toString()));
            }
            claims.put("context", new HashMap<>());

            T t = cn.hutool.core.bean.BeanUtil.mapToBean(claims, clz, false);
//            T t = BeanUtil.mapToBean(clz, claims);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            throw new TokenParseException(e);
        }

    }

    /**
     * 获取过期时间
     *
     * @return
     */
    public long getExpiration() {
        long tokenDate = getFormatDate(claims.getExpiration().toString());
        return tokenDate;
    }


    private long getFormatDate(String dateFormat) {
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);

            return sdf1.parse(dateFormat).getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
