package com.hys.app.framework.context.request;


/**
 * 门店
 *
 * @author 张崧
 * @version v7.0
 * @date 2021-08-26
 * @since v7.0
 */

public class Shop {

    /**
     * 门店id
     */
    private Long shopId;
    /**
     * 门店名称
     */
    private String shopName;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Shop() {
        
    }

    public Shop(Long shopId, String shopName) {
        this.setShopId(shopId);
        this.setShopName(shopName);
    }
}
