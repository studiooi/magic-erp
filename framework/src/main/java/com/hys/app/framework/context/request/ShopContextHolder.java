package com.hys.app.framework.context.request;



/**
 *  用ThreadLocal来存储Session,以便实现Session any where
 * @author kingapex
 * <p>2009-12-17 下午03:10:09</p>
 * @version 1.1
 * 新增request any where
 */
public class ShopContextHolder {


	private static ThreadLocal<Shop> shopThreadLocalHolder  = new ThreadLocal<Shop>();


	public static void setShop(Shop shop){

		shopThreadLocalHolder.set(shop);
	}

	public static Shop getShop(){
		return  shopThreadLocalHolder.get();
	}
}
