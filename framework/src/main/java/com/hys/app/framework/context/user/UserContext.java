package com.hys.app.framework.context.user;

import com.hys.app.framework.context.app.AppTypeContext;
import com.hys.app.framework.context.app.AppTypeEnum;
import com.hys.app.framework.security.model.Buyer;
import com.hys.app.framework.security.model.Seller;

/**
 * 用户上下文
 * Created by kingapex on 2018/3/12.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/12
 */
public class UserContext {

    private static UserHolder userHolder;


    private static UserHolder chainUserHolder;

    public static void setChainHolder(UserHolder user) {
        chainUserHolder = user;
    }

    public static void setHolder(UserHolder user) {
        userHolder = user;
    }

    /**
     * 获取当前卖家
     *
     * @return
     */
    public static Seller getSeller() {
        return userHolder.getSeller();
    }

    /**
     * 为了方便在单元测试中测试已登录的情况，请使用此属性
     * 如果此属性有值，买家上下文中将会直接返回此模拟对象
     */
    public static Buyer mockBuyer = null;

    /**
     * 获取当前买家
     *
     * @return
     */
    public static Buyer getBuyer() {

        //如果有模拟对象，会直接返回此模拟对象
        if (mockBuyer != null) {
            return mockBuyer;
        }

        //**********************************************
        //因为收银端登录后 需要通过会员对象获取结算信息
        //普通会员端登录 则不需要获取结算信息
        //所以这里将两个对象进行区分
        AppTypeEnum appTypeEnum = AppTypeContext.get();

        if (AppTypeEnum.Cashier.equals(appTypeEnum) || AppTypeEnum.Guide.equals(appTypeEnum)) {
            return chainUserHolder.getBuyer();
        } else {
            return userHolder.getBuyer();
        }

    }


}
