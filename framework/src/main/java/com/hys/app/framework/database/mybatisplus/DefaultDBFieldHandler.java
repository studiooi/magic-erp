package com.hys.app.framework.database.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.hys.app.framework.context.user.AdminUserContext;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.framework.util.DateUtil;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Objects;

/**
 * 通用参数填充实现类
 * 如果没有显式的对通用参数进行赋值，这里会对通用参数进行填充、赋值
 */
public class DefaultDBFieldHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        if (Objects.nonNull(metaObject) && metaObject.getOriginalObject() instanceof BaseDO) {
            BaseDO baseDO = (BaseDO) metaObject.getOriginalObject();

            // 创建时间为空，则以当前时间为插入时间
            if (Objects.isNull(baseDO.getCreateTime())) {
                baseDO.setCreateTime(DateUtil.getDateline());
            }
            // 更新时间为空，则以当前时间为更新时间
            if (Objects.isNull(baseDO.getUpdateTime())) {
                baseDO.setUpdateTime(DateUtil.getDateline());
            }

            String adminUserName = AdminUserContext.getAdminUserName();
            // 当前登录用户不为空，创建人为空，则当前登录用户为创建人
            if (Objects.nonNull(adminUserName) && Objects.isNull(baseDO.getCreator())) {
                baseDO.setCreator(adminUserName);
            }
            // 当前登录用户不为空，更新人为空，则当前登录用户为更新人
            if (Objects.nonNull(adminUserName) && Objects.isNull(baseDO.getUpdater())) {
                baseDO.setUpdater(adminUserName);
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新时间为空，则以当前时间为更新时间
        Object modifyTime = getFieldValByName("updateTime", metaObject);
        if (Objects.isNull(modifyTime)) {
            setFieldValByName("updateTime", DateUtil.getDateline(), metaObject);
        }

        // 当前登录用户不为空，更新人为空，则当前登录用户为更新人
        Object modifier = getFieldValByName("updater", metaObject);
        String adminUserName = AdminUserContext.getAdminUserName();
        if (Objects.nonNull(adminUserName) && Objects.isNull(modifier)) {
            setFieldValByName("updater", adminUserName, metaObject);
        }
    }
}
