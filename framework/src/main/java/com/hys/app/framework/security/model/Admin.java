package com.hys.app.framework.security.model;

import cn.hutool.core.map.MapUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理员角色
 *
 * @author zh
 * @version v7.0
 * @date 18/6/27 上午10:09
 * @since v7.0
 */

public class Admin extends User {

    /**
     * 是否是超级管理员
     */
    private Integer founder;


    /**
     * 角色
     */
    private List<String> roles;

    /**
     * 平台仓名称
     */
    private String platformShopName;

    /**
     * 平台仓id
     */
    private Long platformShopId;

    /**
     * AdminUser 维度的临时缓存
     */
    @JsonIgnore
    private Map<String, Object> context;

    public void setContext(String key, Object value) {
        if (context == null) {
            context = new HashMap<>();
        }
        context.put(key, value);
    }

    public <T> T getContext(String key, Class<T> type) {
        return MapUtil.get(context, key, type);
    }

    public Admin() {
    }


    public Integer getFounder() {
        return founder;
    }

    public void setFounder(Integer founder) {
        this.founder = founder;
    }

    public String getPlatformShopName() {
        return platformShopName;
    }

    public void setPlatformShopName(String platformShopName) {
        this.platformShopName = platformShopName;
    }

    public Long getPlatformShopId() {
        return platformShopId;
    }

    public void setPlatformShopId(Long platformShopId) {
        this.platformShopId = platformShopId;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "founder=" + founder +
                ", roles=" + roles +
                ", platformShopName='" + platformShopName + '\'' +
                ", platformShopId=" + platformShopId +
                '}' + super.toString();

    }
}
