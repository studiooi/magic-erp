package com.hys.app.controller.admin.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.EnterpriseDTO;
import com.hys.app.model.erp.dto.EnterpriseQueryParams;
import com.hys.app.model.erp.vo.EnterpriseVO;
import com.hys.app.service.erp.EnterpriseManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 企业 API
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@RestController
@RequestMapping("/admin/erp/enterprise")
@Api(tags = "企业API")
@Validated
public class EnterpriseController {

    @Autowired
    private EnterpriseManager enterpriseManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<EnterpriseVO> list(EnterpriseQueryParams queryParams) {
        return enterpriseManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    public void add(@RequestBody @Valid EnterpriseDTO enterpriseDTO) {
        enterpriseManager.add(enterpriseDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    public void edit(@PathVariable Long id, @RequestBody @Valid EnterpriseDTO enterpriseDTO) {
        enterpriseDTO.setId(id);
        enterpriseManager.edit(enterpriseDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public EnterpriseVO getDetail(@PathVariable Long id) {
        return enterpriseManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    public void delete(@PathVariable List<Long> ids) {
        enterpriseManager.delete(ids);
    }

}

