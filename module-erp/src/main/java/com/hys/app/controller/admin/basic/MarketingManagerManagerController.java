package com.hys.app.controller.admin.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.MarketingManagerDTO;
import com.hys.app.model.erp.dto.MarketingManagerQueryParams;
import com.hys.app.model.erp.vo.MarketingManagerVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.MarketingManagerManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 营销经理相关API
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@RestController
@RequestMapping("/admin/erp/marketingManager")
@Api(tags = "营销经理相关API")
@Validated
public class MarketingManagerManagerController {

    @Autowired
    private MarketingManagerManager marketingManagerManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<MarketingManagerVO> list(MarketingManagerQueryParams queryParams) {
        return marketingManagerManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增销售经理")
    public void add(@RequestBody @Valid MarketingManagerDTO marketingManagerDTO) {
        marketingManagerManager.add(marketingManagerDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的销售经理")
    public void edit(@PathVariable Long id, @RequestBody @Valid MarketingManagerDTO marketingManagerDTO) {
        marketingManagerDTO.setId(id);
        marketingManagerManager.edit(marketingManagerDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public MarketingManagerVO getDetail(@PathVariable Long id) {
        return marketingManagerManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除id为[${ids}]的销售经理")
    public void delete(@PathVariable List<Long> ids) {
        marketingManagerManager.delete(ids);
    }


}

