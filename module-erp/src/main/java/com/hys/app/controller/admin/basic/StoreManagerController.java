package com.hys.app.controller.admin.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StoreQueryParams;
import com.hys.app.model.erp.vo.StoreVO;
import com.hys.app.model.erp.dto.StoreDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.StoreManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 门店API
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@RestController
@RequestMapping("/admin/erp/store")
@Api(tags = "门店API")
@Validated
public class StoreManagerController {

    @Autowired
    private StoreManager storeManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<StoreVO> list(StoreQueryParams queryParams) {
        return storeManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增门店")
    public void add(@RequestBody @Valid StoreDTO storeDTO) {
        storeManager.add(storeDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的门店")
    public void edit(@PathVariable Long id, @RequestBody @Valid StoreDTO storeDTO) {
        storeDTO.setId(id);
        storeManager.edit(storeDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public StoreVO getDetail(@PathVariable Long id) {
        return storeManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    @Log(client = LogClient.admin, detail = "删除id为[${id}]的门店")
    public void delete(@PathVariable Long id) {
        storeManager.delete(id);
    }
}

