package com.hys.app.controller.admin.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseQueryParams;
import com.hys.app.model.erp.vo.WarehouseVO;
import com.hys.app.model.erp.dto.WarehouseDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.WarehouseManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 仓库相关API
 *
 * @author 张崧
 * 2023-11-30 16:35:16
 */
@RestController
@RequestMapping("/admin/erp/warehouse")
@Api(tags = "仓库相关API")
@Validated
public class WarehouseManagerController {

    @Autowired
    private WarehouseManager warehouseManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseVO> list(WarehouseQueryParams queryParams) {
        return warehouseManager.list(queryParams);
    }

    @ApiOperation(value = "查询所有仓库")
    @GetMapping("/list-all")
    public List<WarehouseVO> listAll(Long deptId) {
        return warehouseManager.listAll(deptId);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "添加名称为[${warehouseDTO.name}]的仓库信息")
    public void add(@RequestBody @Valid WarehouseDTO warehouseDTO) {
        warehouseManager.add(warehouseDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "编辑ID为[${id}]的仓库信息")
    public void edit(@PathVariable Long id, @RequestBody @Valid WarehouseDTO warehouseDTO) {
        warehouseDTO.setId(id);
        warehouseManager.edit(warehouseDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public WarehouseVO getDetail(@PathVariable Long id) {
        return warehouseManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    @Log(client = LogClient.admin, detail = "删除ID为[${id}]的仓库信息")
    public void delete(@PathVariable Long id) {
        warehouseManager.delete(id);
    }
}

