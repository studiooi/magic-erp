package com.hys.app.controller.admin.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.GoodsLabelRelationDTO;
import com.hys.app.model.erp.dto.GoodsLabelRelationQueryParams;
import com.hys.app.model.erp.vo.GoodsLabelRelationVO;
import com.hys.app.service.erp.GoodsLabelRelationManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 商品标签-商品关联表 API
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@RestController
@RequestMapping("/admin/erp/goodsLabelRelation")
@Api(tags = "商品标签-商品关联表API")
@Validated
public class GoodsLabelRelationController {

    @Autowired
    private GoodsLabelRelationManager goodsLabelRelationManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<GoodsLabelRelationVO> list(GoodsLabelRelationQueryParams queryParams) {
        return goodsLabelRelationManager.list(queryParams);
    }

    @ApiOperation(value = "关联商品")
    @PostMapping("/relationGoods")
    public void relationGoods(@RequestBody @Valid GoodsLabelRelationDTO goodsLabelRelationDTO) {
        goodsLabelRelationManager.relationGoods(goodsLabelRelationDTO);
    }

}

