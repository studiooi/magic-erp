package com.hys.app.controller.admin.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.GoodsDTO;
import com.hys.app.model.erp.dto.GoodsQueryParams;
import com.hys.app.model.erp.vo.GoodsVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.GoodsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品档案API
 *
 * @author 张崧
 * 2023-12-26 15:56:58
 */
@RestController
@RequestMapping("/admin/erp/goods")
@Api(tags = "商品档案API")
@Validated
public class GoodsManagerController {

    @Autowired
    private GoodsManager goodsManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<GoodsVO> list(GoodsQueryParams queryParams) {
        return goodsManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增商品")
    public void add(@RequestBody @Valid GoodsDTO goodsDTO) {
        goodsManager.add(goodsDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的商品")
    public void edit(@PathVariable Long id, @RequestBody @Valid GoodsDTO goodsDTO) {
        goodsDTO.setId(id);
        goodsManager.edit(goodsDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public GoodsVO getDetail(@PathVariable Long id) {
        return goodsManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除id为[${ids}]的商品")
    public void delete(@PathVariable List<Long> ids) {
        goodsManager.delete(ids);
    }
}

