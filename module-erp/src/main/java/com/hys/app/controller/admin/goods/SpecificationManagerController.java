package com.hys.app.controller.admin.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.model.errorcode.GoodsErrorCode;
import com.hys.app.model.goods.dos.SpecValuesDO;
import com.hys.app.model.goods.dos.SpecificationDO;
import com.hys.app.model.goods.vo.SpecificationVO;
import com.hys.app.service.goods.SpecValuesManager;
import com.hys.app.service.goods.SpecificationManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 规格项控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 09:31:27
 */
@RestController
@RequestMapping("/admin/goods/specs")
@Api(description = "规格项相关API")
public class SpecificationManagerController {

	@Autowired
	private SpecificationManager specificationManager;

	@Autowired
	private SpecValuesManager specValuesManager;

	@ApiOperation(value = "查询规格项列表", response = SpecificationDO.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "规格名称", dataType = "string", paramType = "query")
			})
	@GetMapping
	public WebPage list(@ApiIgnore @NotEmpty(message = "页码不能为空") Long pageNo,
                        @ApiIgnore @NotEmpty(message = "每页数量不能为空") Long pageSize, String keyword) {

		return this.specificationManager.list(pageNo, pageSize, keyword);
	}

	@ApiOperation(value = "添加规格项", response = SpecificationDO.class)
	@PostMapping
	public SpecificationDO add(@Valid SpecificationDO specification) {
		// 平台的规格
		specification.setSellerId(0L);
		this.specificationManager.add(specification);

		return specification;
	}

	@PutMapping(value = "/{id}")
	@ApiOperation(value = "修改规格项", response = SpecificationDO.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "int", paramType = "path") })
	public SpecificationDO edit(@Valid SpecificationDO specification, @PathVariable Long id) {

		this.specificationManager.edit(specification, id);

		return specification;
	}

	@DeleteMapping(value = "/{ids}")
	@ApiOperation(value = "删除规格项")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "ids", value = "要删除的规格项主键", required = true, dataType = "int", paramType = "path",allowMultiple=true) })
	public String delete(@PathVariable Long[] ids) {

		this.specificationManager.delete(ids);

		return "";
	}

	@GetMapping(value = "/{id}")
	@ApiOperation(value = "查询一个规格项")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "要查询的规格项主键", required = true, dataType = "int", paramType = "path") })
	public SpecificationDO get(@PathVariable Long id) {

		SpecificationDO specification = this.specificationManager.getModel(id);

		return specification;
	}

	@ApiOperation(value = "根据分类id查询规格信息(包括规格值)", notes = "根据分类id查询规格")
	@ApiImplicitParam(name = "category_id", value = "分类id", required = true, paramType = "path", dataType = "int")
	@GetMapping("/categories/{category_id}")
	public List<SpecificationVO> queryAdminSpec(@PathVariable("category_id") Long categoryId) {

		return this.specificationManager.queryAdminSpec(categoryId);
	}

	@ApiOperation(value = "平台自定义某分类的规格项", response = SpecificationDO.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "category_id", value = "分类id", required = true, paramType = "path", dataType = "int"),
			@ApiImplicitParam(name = "spec_name", value = "规格项名称", required = true, paramType = "query", dataType = "string")})
	@PostMapping("/categories/{category_id}")
	public SpecificationDO addAdminSpec(@PathVariable("category_id") Long categoryId,
							   @ApiIgnore @NotEmpty(message = "规格名称不能为空") @Length(max = 50, message = "规格名称不能超过50个字符") String specName) {
		// 商家的规格
		SpecificationDO specification = this.specificationManager.addSellerSpec(categoryId, specName);

		return specification;
	}

	@ApiOperation(value = "平台自定义某规格的规格值")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "spec_id", value = "规格id", required = true, paramType = "path", dataType = "int"),
			@ApiImplicitParam(name = "value_name", value = "规格值", required = true, paramType = "query", dataType = "string")})
	@PostMapping("/categories/{spec_id}/values")
	public SpecValuesDO addValue(@PathVariable("spec_id") Long specId,
								 @ApiIgnore @NotEmpty(message = "规格值名称不能为空") @Length(max = 50,message = "规格值名称不能超过50个字符") String valueName) {
		SpecificationDO spec = specificationManager.getModel(specId);
		if (spec == null) {
			throw new ServiceException(GoodsErrorCode.E306.code(), "无权操作");
		}
		if (specificationManager.flagSellerSpec(0L, specId, valueName)) {
			throw new ServiceException(GoodsErrorCode.E306.code(), "该规格值已存在 ！");
		}
		SpecValuesDO value = new SpecValuesDO(specId, valueName, 0L);

		value.setSpecName(spec.getSpecName());

		this.specValuesManager.add(value);

		return value;
	}
}
