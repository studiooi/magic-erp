package com.hys.app.controller.admin.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.ProcurementPlan;
import com.hys.app.model.erp.dos.ProcurementPlanProduct;
import com.hys.app.model.erp.dto.ProcurementPlanDTO;
import com.hys.app.model.erp.dto.ProcurementPlanQueryParam;
import com.hys.app.model.erp.vo.ProcurementPlanVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.ProcurementPlanManager;
import com.hys.app.service.erp.ProcurementPlanProductManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购计划相关API
 * @author dmy
 * 2023-12-05
 */
@Api(description = "采购计划相关API")
@RestController
@RequestMapping("/admin/procurement/plan")
@Validated
public class ProcurementPlanManagerController {

    @Autowired
    private ProcurementPlanManager procurementPlanManager;

    @Autowired
    private ProcurementPlanProductManager procurementPlanProductManager;

    @ApiOperation(value = "查询采购计划分页列表数据")
    @GetMapping
    public WebPage list(ProcurementPlanQueryParam params) {
        return procurementPlanManager.list(params);
    }

    @ApiOperation(value = "新增采购计划")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增采购计划")
    public void add(@RequestBody @Valid ProcurementPlanDTO procurementPlanDTO) {
        this.procurementPlanManager.add(procurementPlanDTO);
    }

    @ApiOperation(value = "修改采购计划")
    @PostMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的采购计划")
    public void edit(@PathVariable Long id, @RequestBody @Valid ProcurementPlanDTO procurementPlanDTO) {
        this.procurementPlanManager.edit(id, procurementPlanDTO);
    }

    @ApiOperation(value = "批量删除采购计划")
    @DeleteMapping("/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的采购计划ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的采购计划")
    public void delete(@PathVariable List<Long> ids) {
        this.procurementPlanManager.delete(ids);
    }

    @ApiOperation(value = "查询采购计划详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "采购计划ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{id}")
    public ProcurementPlanVO getDetail(@PathVariable Long id) {
        return procurementPlanManager.getDetail(id);
    }

    @ApiOperation(value = "获取所有采购计划信息集合")
    @GetMapping("/list-all")
    public List<ProcurementPlan> listAll() {
        return procurementPlanManager.listAll();
    }

    @ApiOperation(value = "根据采购计划ID查询采购计划商品信息集合")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "采购计划ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/product/{id}")
    public List<ProcurementPlanProduct> listProductById(@PathVariable Long id) {
        return procurementPlanProductManager.list(id);
    }
}
