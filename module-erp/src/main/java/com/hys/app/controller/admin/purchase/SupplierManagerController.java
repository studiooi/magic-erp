package com.hys.app.controller.admin.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.SupplierDTO;
import com.hys.app.model.erp.dto.SupplierQueryParams;
import com.hys.app.model.erp.vo.SupplierVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.SupplierManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 供应商相关API
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
@RestController
@RequestMapping("/admin/erp/supplier")
@Api(tags = "供应商相关API")
@Validated
public class SupplierManagerController {

    @Autowired
    private SupplierManager supplierManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<SupplierVO> list(SupplierQueryParams queryParams) {
        return supplierManager.list(queryParams);
    }

    @ApiOperation(value = "所有正常状态的供应商")
    @GetMapping("/list-all")
    public List<SupplierVO> normalStatusList() {
        return supplierManager.normalStatusList();
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "添加名称为[${supplierDTO.customName}]的供应商")
    public void add(@RequestBody @Valid SupplierDTO supplierDTO) {
        supplierManager.add(supplierDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的供应商信息")
    public void edit(@PathVariable Long id, @RequestBody @Valid SupplierDTO supplierDTO) {
        supplierDTO.setId(id);
        supplierManager.edit(supplierDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public SupplierVO getDetail(@PathVariable Long id) {
        return supplierManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    @Log(client = LogClient.admin, detail = "删除ID为[${id}]的供应商信息")
    public void delete(@PathVariable Long id) {
        supplierManager.delete(id);
    }

    @ApiOperation(value = "禁用/恢复")
    @PutMapping("/{id}/update-disable")
    @Log(client = LogClient.admin, detail = "${disable ? '恢复' : '禁用'}ID为[${id}]的供应商信息")
    public void updateDisable(@PathVariable Long id, @RequestParam("disable") Boolean disable) {
        supplierManager.updateDisable(id, disable);
    }

}

