package com.hys.app.controller.admin.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.SupplierSettlementQueryParams;
import com.hys.app.model.erp.vo.SupplierSettlementVO;
import com.hys.app.model.erp.dto.SupplierSettlementDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.SupplierSettlementManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 供应商结算单API
 *
 * @author 张崧
 * @since 2023-12-15 14:09:09
 */
@RestController
@RequestMapping("/admin/erp/supplierSettlement")
@Api(tags = "供应商结算单API")
@Validated
public class SupplierSettlementManagerController {

    @Autowired
    private SupplierSettlementManager supplierSettlementManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<SupplierSettlementVO> list(SupplierSettlementQueryParams queryParams) {
        return supplierSettlementManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建供应商结算单")
    public void add(@RequestBody @Valid SupplierSettlementDTO supplierSettlementDTO) {
        supplierSettlementManager.add(supplierSettlementDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public SupplierSettlementVO getDetail(@PathVariable Long id) {
        return supplierSettlementManager.getDetail(id);
    }

//    @ApiOperation(value = "删除")
//    @DeleteMapping("/{ids}")
//    public void delete(@PathVariable List<Long> ids) {
//        supplierSettlementManager.delete(ids);
//    }

    @ApiOperation(value = "取消结算单")
    @DeleteMapping("/{id}/cancel")
    @Log(client = LogClient.admin, detail = "取消id为[${id}]的供应商结算单")
    public void cancel(@PathVariable Long id) {
        supplierSettlementManager.cancel(id);
    }

}

