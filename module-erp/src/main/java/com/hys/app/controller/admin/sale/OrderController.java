package com.hys.app.controller.admin.sale;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.dto.OrderQueryParams;
import com.hys.app.model.erp.enums.OrderSourceEnum;
import com.hys.app.model.erp.vo.OrderVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.OrderManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 订单 API
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@RestController
@RequestMapping("/admin/erp/order")
@Api(tags = "订单API")
@Validated
public class OrderController {

    @Autowired
    private OrderManager orderManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<OrderVO> list(OrderQueryParams queryParams) {
        return orderManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建订单")
    public void add(@RequestBody @Valid OrderDTO orderDTO) {
        setSource(orderDTO);
        orderManager.add(orderDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的订单")
    public void edit(@PathVariable Long id, @RequestBody @Valid OrderDTO orderDTO) {
        orderDTO.setId(id);
        setSource(orderDTO);
        orderManager.edit(orderDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public OrderVO getDetail(@PathVariable Long id) {
        return orderManager.getDetail(id);
    }

    @ApiOperation(value = "提交")
    @PostMapping("/{ids}/submit")
    @Log(client = LogClient.admin, detail = "提交ID为[${ids}]的订单")
    public void submit(@PathVariable List<Long> ids) {
        orderManager.submit(ids);
    }

    @ApiOperation(value = "撤销提交")
    @PostMapping("/{ids}/withdraw")
    @Log(client = LogClient.admin, detail = "撤销提交ID为[${ids}]的订单")
    public void withdraw(@PathVariable List<Long> ids) {
        orderManager.withdraw(ids);
    }

    @ApiOperation(value = "审核")
    @PostMapping("/{ids}/audit")
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的订单")
    public void audit(@PathVariable List<Long> ids, @NotNull Boolean isPass, String remark) {
        orderManager.audit(ids, isPass, remark);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除id为[${ids}]的订单")
    public void delete(@PathVariable List<Long> ids) {
        orderManager.delete(ids);
    }

    @ApiOperation(value = "标记为已支付")
    @PostMapping("/{ids}/payment")
    @Log(client = LogClient.admin, detail = "将id为[${ids}]的订单标记为已支付")
    public void payment(@PathVariable List<Long> ids) {
        orderManager.payment(ids);
    }


    private void setSource(OrderDTO orderDTO) {
        orderDTO.setSource(OrderSourceEnum.Internal);
        orderDTO.setSourceId("0");
    }
}

