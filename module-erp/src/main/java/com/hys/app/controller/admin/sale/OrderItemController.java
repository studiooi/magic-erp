package com.hys.app.controller.admin.sale;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderItemQueryParams;
import com.hys.app.model.erp.vo.OrderItemVO;
import com.hys.app.service.erp.OrderItemManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单明细 API
 *
 * @author 张崧
 * 2024-01-24 16:39:39
 */
@RestController
@RequestMapping("/admin/erp/orderItem")
@Api(tags = "订单明细API")
@Validated
public class OrderItemController {

    @Autowired
    private OrderItemManager orderItemManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<OrderItemVO> list(OrderItemQueryParams queryParams) {
        return orderItemManager.list(queryParams);
    }

}

