package com.hys.app.controller.admin.system;

import com.hys.app.converter.system.DictDataConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.model.system.dos.DictDataDO;
import com.hys.app.model.system.dto.DictDataCreateDTO;
import com.hys.app.model.system.dto.DictDataQueryParams;
import com.hys.app.model.system.dto.DictDataUpdateDTO;
import com.hys.app.model.system.vo.DictDataRespVO;
import com.hys.app.model.system.vo.DictDataSimpleRespVO;
import com.hys.app.service.system.DictDataManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "管理后台 - 字典数据")
@RestController
@RequestMapping("/admin/systems/dict-data")
@Validated
public class DictDataController {

    @Resource
    private DictDataManager dictDataManager;

    @Autowired
    private DictDataConverter converter;

    @PostMapping("/create")
    @ApiOperation(value = "新增字典数据")
    @Log(client = LogClient.admin, detail = "新增标签为[${reqVO.label}]的字典数据信息")
    public void createDictData(@Valid @RequestBody DictDataCreateDTO reqVO) {
        dictDataManager.createDictData(reqVO);
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改字典数据")
    @Log(client = LogClient.admin, detail = "修改标签为[${reqVO.label}]的字典数据信息")
    public void updateDictData(@Valid @RequestBody DictDataUpdateDTO reqVO) {
        dictDataManager.updateDictData(reqVO);
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "删除字典数据")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024")
    @Log(client = LogClient.admin, detail = "删除ID为[${id}]的字典数据信息")
    public void deleteDictData(Long id) {
        dictDataManager.deleteDictData(id);
    }

    @GetMapping("/list-all-simple")
    @ApiOperation(value = "获得全部字典数据列表", notes = "一般用于管理后台缓存字典数据在本地")
    public List<DictDataSimpleRespVO> getSimpleDictDataList(String dictType) {
        List<DictDataDO> list = dictDataManager.getDictDataList(dictType);
        return converter.convertList(list);
    }

    @GetMapping("/page")
    @ApiOperation(value = "获得字典类型的分页列表")
    public WebPage<DictDataRespVO> getDictTypePage(@Valid DictDataQueryParams reqVO) {
        return converter.convertPage(dictDataManager.getDictDataPage(reqVO));
    }

    @GetMapping(value = "/get")
    @ApiOperation(value = "/查询字典数据详细")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024")
    public DictDataRespVO getDictData(@RequestParam("id") Long id) {
        return converter.convert(dictDataManager.getDictData(id));
    }

}
