package com.hys.app.controller.admin.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.model.errorcode.SystemErrorCode;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.model.system.dos.SmsTemplate;
import com.hys.app.service.system.SmsTemplateManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotBlank;

/**
 * 短信模板控制器
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
@RestController
@RequestMapping("/admin/systems/sms-template")
@Api(tags = "短信模板相关API")
public class SmsTemplateManagerController {

    @Autowired
    private SmsTemplateManager smsTemplateManager;


    @ApiOperation(value = "查询短信模板列表", response = SmsTemplate.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "service_type", value = "业务类型", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query", allowableValues = "SHOP,MEMBER"),
            @ApiImplicitParam(name = "bean_id", value = "短信平台beanId", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String serviceType, @ApiIgnore String beanId) {

        return this.smsTemplateManager.list(pageNo, pageSize, serviceType, beanId);
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个短信模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的短信模板主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path")
    })
    public SmsTemplate get(@PathVariable Long id) {

        SmsTemplate smsTemplate = this.smsTemplateManager.getModel(id);

        return smsTemplate;
    }

    @PutMapping(value = "/{id}/enable-status")
    @ApiOperation(value = "修改短信模板开启状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
            @ApiImplicitParam(name = "enable_status", value = "开启状态", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query", allowableValues = "OPEN,CLOSE")
    })
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的短信模板开启状态")
    public void updateEnableStatus(@PathVariable Long id, @ApiIgnore String enableStatus) {

        this.smsTemplateManager.updateEnableStatus(id, enableStatus);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改短信模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
            @ApiImplicitParam(name = "content", value = "短信模板内容", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的短信模板内容")
    public void edit(@PathVariable Long id, @ApiIgnore @NotBlank(message = "短信模板内容不能为空") String content) {

        //设置要修改的字段（可以修改的字段：模板名称，模板内容，模板类型（短信通知，验证码））
        SmsTemplate smsTemplate = new SmsTemplate();
        smsTemplate.setTemplateContent(content);

        this.smsTemplateManager.edit(smsTemplate, id);
    }

    @GetMapping(value = "/{id}/audit")
    @ApiOperation(value = "提交审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
            @ApiImplicitParam(name = "remark", value = "申请说明(描述业务使用场景)", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @Log(client = LogClient.admin, detail = "将ID为[${id}]的短信模板提交审核")
    public void audit(@PathVariable Long id, @ApiIgnore String remark) {

        if (remark.length() > 30) {
            throw new ServiceException(SystemErrorCode.E931.code(), "申请说明不能超过30个字符");
        }

        this.smsTemplateManager.audit(id, remark);
    }

}