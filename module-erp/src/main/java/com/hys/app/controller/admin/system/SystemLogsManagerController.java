package com.hys.app.controller.admin.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.SystemLogs;
import com.hys.app.model.system.dto.SystemLogsParam;
import com.hys.app.service.system.SystemLogsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 系统日志控制器
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:59
 */
@RestController
@RequestMapping("/admin/system-logs")
@Api(description = "系统日志相关API")
@Validated
public class SystemLogsManagerController {
	
	@Autowired
	private SystemLogsManager systemLogsManager;

	@ApiOperation(value = "查询系统日志列表", response = SystemLogs.class)
	@GetMapping
	public WebPage<SystemLogs> list(@Valid SystemLogsParam param)	{
		return	this.systemLogsManager.list(param,"admin");
	}

	@GetMapping(value =	"/{id}")
	@ApiOperation(value = "查询一个系统日志", response = SystemLogs.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "要查询的系统日志主键", required = true, dataType = "int", paramType = "path")
	})
	public SystemLogs get(@PathVariable Long id)	{
		SystemLogs systemLogs = this.systemLogsManager.getModel(id);
		return	systemLogs;
	}
}