package com.hys.app.controller.admin.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dto.TaskQueryParams;
import com.hys.app.model.system.vo.TaskVO;
import com.hys.app.service.system.TaskManager;
import com.hys.app.service.system.task.TaskExecutor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 任务 API
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
@RestController
@RequestMapping("/admin/systems/task")
@Api(tags = "任务API")
@Validated
public class TaskController {

    @Autowired
    private TaskManager taskManager;

    @Autowired
    private TaskExecutor taskExecutor;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<TaskVO> list(TaskQueryParams queryParams) {
        return taskManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public TaskVO getDetail(@PathVariable Long id) {
        return taskManager.getDetail(id);
    }

    @ApiOperation(value = "终止")
    @GetMapping("/{id}/stop")
    public void stop(@PathVariable Long id) {
        taskExecutor.stop(id);
    }

}

