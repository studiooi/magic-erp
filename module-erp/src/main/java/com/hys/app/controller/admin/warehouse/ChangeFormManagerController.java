package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.ChangeFormDTO;
import com.hys.app.model.erp.dto.ChangeFormQueryParam;
import com.hys.app.model.erp.dto.ChangeFormStatisticsQueryParam;
import com.hys.app.model.erp.vo.ChangeFormVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.ChangeFormManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 换货单相关API
 * @author dmy
 * 2023-12-05
 */
@Api(description = "换货单相关API")
@RestController
@RequestMapping("/admin/change/form")
@Validated
public class ChangeFormManagerController {

    @Autowired
    private ChangeFormManager changeFormManager;

    @ApiOperation(value = "查询换货单分页列表数据")
    @GetMapping
    public WebPage list(ChangeFormQueryParam params) {
        return changeFormManager.list(params);
    }

    @ApiOperation(value = "新增换货单")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增换货单")
    public void add(@RequestBody @Valid ChangeFormDTO changeFormDTO) {
        this.changeFormManager.add(changeFormDTO);
    }

    @ApiOperation(value = "修改换货单")
    @PostMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的换货单")
    public void edit(@PathVariable Long id, @RequestBody @Valid ChangeFormDTO changeFormDTO) {
        this.changeFormManager.edit(id, changeFormDTO);
    }

    @ApiOperation(value = "批量删除换货单")
    @DeleteMapping("/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的换货单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的换货单")
    public void delete(@PathVariable List<Long> ids) {
        this.changeFormManager.delete(ids);
    }

    @ApiOperation(value = "查询换货单详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "换货单ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{id}")
    public ChangeFormVO getDetail(@PathVariable Long id) {
        return changeFormManager.getDetail(id);
    }

    @ApiOperation(value = "审核换货单")
    @PostMapping("/audit/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要审核的换货单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true),
            @ApiImplicitParam(name = "status", value = "审核状态 PASS：审核通过，REJECT：审核驳回", required = true, dataType = "String", paramType = "query", allowableValues = "PASS,REJECT"),
            @ApiImplicitParam(name = "reject_reason", value = "驳回原因", dataType = "String", paramType = "query")
    })
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的换货单")
    public void audit(@PathVariable List<Long> ids, @ApiIgnore String status, @ApiIgnore String rejectReason) {
        this.changeFormManager.audit(ids, status, rejectReason);
    }

    @ApiOperation(value = "提交换货单")
    @PostMapping("/submit/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要提交的换货单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "将ID为[${ids}]的换货单提交审核")
    public void submit(@PathVariable List<Long> ids) {
        this.changeFormManager.submit(ids);
    }

    @ApiOperation(value = "撤销换货单")
    @PostMapping("/cancel/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要撤销的换货单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "将ID为[${ids}]的换货单撤销提交审核")
    public void cancel(@PathVariable List<Long> ids) {
        this.changeFormManager.cancel(ids);
    }

    @ApiOperation(value = "查询商品换货单商品统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(ChangeFormStatisticsQueryParam params) {
        return changeFormManager.statistics(params);
    }

    @ApiOperation(value = "导出商品换货单商品列表")
    @GetMapping("/export")
    public void export(HttpServletResponse response, ChangeFormStatisticsQueryParam params) {
        this.changeFormManager.export(response, params);
    }
}
