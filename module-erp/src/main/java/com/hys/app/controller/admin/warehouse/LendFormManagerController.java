package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.LendFormDTO;
import com.hys.app.model.erp.dto.LendFormQueryParam;
import com.hys.app.model.erp.dto.LendFormReturnDTO;
import com.hys.app.model.erp.dto.ProductLendNumDTO;
import com.hys.app.model.erp.vo.LendFormVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.LendFormManager;
import com.hys.app.service.erp.LendFormProductManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品借出相关API
 * @author dmy
 * 2023-12-05
 */
@Api(description = "商品借出相关API")
@RestController
@RequestMapping("/admin/lend/form")
@Validated
public class LendFormManagerController {

    @Autowired
    private LendFormManager lendFormManager;

    @Autowired
    private LendFormProductManager lendFormProductManager;

    @ApiOperation(value = "查询商品借出单分页列表数据")
    @GetMapping
    public WebPage list(LendFormQueryParam params) {
        return lendFormManager.list(params);
    }

    @ApiOperation(value = "新增商品借出单")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增商品借出单")
    public void add(@RequestBody @Valid LendFormDTO lendFormDTO) {
        this.lendFormManager.add(lendFormDTO);
    }

    @ApiOperation(value = "修改商品借出单")
    @PostMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的商品借出单")
    public void edit(@PathVariable Long id, @RequestBody @Valid LendFormDTO lendFormDTO) {
        this.lendFormManager.edit(id, lendFormDTO);
    }

    @ApiOperation(value = "批量删除商品借出单")
    @DeleteMapping("/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的商品借出单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的商品借出单")
    public void delete(@PathVariable List<Long> ids) {
        this.lendFormManager.delete(ids);
    }

    @ApiOperation(value = "查询商品借出单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "商品借出单ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{id}")
    public LendFormVO getDetail(@PathVariable Long id) {
        return lendFormManager.getDetail(id);
    }

    @ApiOperation(value = "批量确认商品借出单")
    @PostMapping("/confirm/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要确认的商品借出单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "批量确认ID为[${ids}]的商品借出单")
    public void confirm(@PathVariable List<Long> ids) {
        this.lendFormManager.confirm(ids);
    }

    @ApiOperation(value = "商品借出单产品归还")
    @PostMapping("/return/{id}")
    @Log(client = LogClient.admin, detail = "id为[${id}]的商品借出单")
    public void returnProduct(@PathVariable Long id, @RequestBody @Valid LendFormReturnDTO lendFormReturnDTO) {
        this.lendFormManager.returnProduct(id, lendFormReturnDTO);
    }

    @ApiOperation(value = "查询待归还的借出单商品分页列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "分页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页数量", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/list-wait-return")
    public WebPage listWaitReturnProductPage(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize) {
        return lendFormProductManager.listWaitReturnProductPage(pageNo, pageSize);
    }

    @ApiOperation(value = "查询产品借出数量信息")
    @PostMapping("/list-product-lend-num")
    public List<ProductLendNumDTO> listProductLendNum(@RequestBody @Valid List<ProductLendNumDTO> lendList) {
        return lendFormProductManager.listProductLendNum(lendList);
    }
}
