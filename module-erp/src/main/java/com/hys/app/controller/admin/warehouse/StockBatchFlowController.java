package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockBatchFlowQueryParams;
import com.hys.app.model.erp.vo.StockBatchFlowVO;
import com.hys.app.service.erp.StockBatchFlowManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 库存批次流水 API
 *
 * @author 张崧
 * 2024-01-16 11:39:01
 */
@RestController
@RequestMapping("/admin/erp/stockBatchFlow")
@Api(tags = "库存批次流水API")
@Validated
public class StockBatchFlowController {

    @Autowired
    private StockBatchFlowManager stockBatchFlowManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<StockBatchFlowVO> list(StockBatchFlowQueryParams queryParams) {
        return stockBatchFlowManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public StockBatchFlowVO getDetail(@PathVariable Long id) {
        return stockBatchFlowManager.getDetail(id);
    }
}

