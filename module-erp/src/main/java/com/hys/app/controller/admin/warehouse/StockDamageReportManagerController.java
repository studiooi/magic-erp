package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockDamageReportDTO;
import com.hys.app.model.erp.dto.StockDamageReportParam;
import com.hys.app.model.erp.dto.StockDamageReportStatisticsParam;
import com.hys.app.model.erp.vo.StockDamageReportVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.StockDamageReportManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 库存报损单相关API
 *
 * @author dmy
 * 2023-12-05
 */
@Api(description = "库存报损单相关API")
@RestController
@RequestMapping("/admin/stock/damage/report")
@Validated
public class StockDamageReportManagerController {

    @Autowired
    private StockDamageReportManager stockDamageReportManager;

    @ApiOperation(value = "查询库存报损单分页列表数据")
    @GetMapping
    public WebPage list(StockDamageReportParam params) {
        return stockDamageReportManager.list(params);
    }

    @ApiOperation(value = "新增库存报损单")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增库存报损单")
    public void add(@RequestBody @Valid StockDamageReportDTO stockDamageReportDTO) {
        this.stockDamageReportManager.add(stockDamageReportDTO);
    }

    @ApiOperation(value = "修改库存报损单")
    @PostMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的库存报损单")
    public void edit(@PathVariable Long id, @RequestBody @Valid StockDamageReportDTO stockDamageReportDTO) {
        this.stockDamageReportManager.edit(id, stockDamageReportDTO);
    }

    @ApiOperation(value = "批量删除库存报损单")
    @DeleteMapping("/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的库存报损单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的库存报损单")
    public void delete(@PathVariable List<Long> ids) {
        this.stockDamageReportManager.delete(ids);
    }

    @ApiOperation(value = "查询库存报损单详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "库存报损单ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{id}")
    public StockDamageReportVO getDetail(@PathVariable Long id) {
        return stockDamageReportManager.getDetail(id);
    }

    @ApiOperation(value = "批量提交审核库存报损单")
    @PostMapping("/submit/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要提交审核的库存报损单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "提交审核ID为[${ids}]的库存报损单")
    public void submit(@PathVariable List<Long> ids) {
        this.stockDamageReportManager.submit(ids);
    }

    @ApiOperation(value = "批量撤销提交审核的库存报损单")
    @PostMapping("/cancel/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要撤销提交审核的库存报损单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "将ID为[${ids}]的库存报损单撤销提交审核")
    public void cancel(@PathVariable List<Long> ids) {
        this.stockDamageReportManager.cancel(ids);
    }

    @ApiOperation(value = "审核库存报损单")
    @PostMapping("/audit/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要审核的库存报损单ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true),
            @ApiImplicitParam(name = "status", value = "审核状态 PASS：审核通过，REJECT：审核驳回", required = true, dataType = "String", paramType = "query", allowableValues = "PASS,REJECT"),
            @ApiImplicitParam(name = "reject_reason", value = "驳回原因", dataType = "String", paramType = "query")
    })
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的库存报损单")
    public void audit(@PathVariable List<Long> ids, @ApiIgnore String status, @ApiIgnore String rejectReason) {
        this.stockDamageReportManager.audit(ids, status, rejectReason);
    }

    @ApiOperation(value = "查询库存报损单商品统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(StockDamageReportStatisticsParam params) {
        return stockDamageReportManager.statistics(params);
    }

    @ApiOperation(value = "导出库存报损单商品统计列表")
    @GetMapping("/export")
    public void export(HttpServletResponse response, StockDamageReportStatisticsParam params) {
        this.stockDamageReportManager.export(response, params);
    }
}
