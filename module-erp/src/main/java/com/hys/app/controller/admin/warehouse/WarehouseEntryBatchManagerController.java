package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseEntryBatchQueryParams;
import com.hys.app.model.erp.vo.WarehouseEntryBatchVO;
import com.hys.app.service.erp.WarehouseEntryBatchManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 入库批次相关API
 *
 * @author 张崧
 * @since 2023-12-08 11:49:52
 */
@RestController
@RequestMapping("/admin/erp/warehouseEntryBatch")
@Api(tags = "入库批次相关API")
@Validated
public class WarehouseEntryBatchManagerController {

    @Autowired
    private WarehouseEntryBatchManager warehouseEntryBatchManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseEntryBatchVO> list(WarehouseEntryBatchQueryParams queryParams) {
        return warehouseEntryBatchManager.list(queryParams);
    }

    @ApiOperation(value = "查询所有批次")
    @GetMapping("/list-all")
    public List<WarehouseEntryBatchVO> listAll(WarehouseEntryBatchQueryParams queryParams) {
        return warehouseEntryBatchManager.listAll(queryParams);
    }

}

