package com.hys.app.controller.admin.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseOutItemQueryParams;
import com.hys.app.model.erp.vo.WarehouseOutItemVO;
import com.hys.app.service.erp.WarehouseOutItemManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 出库单明细相关API
 *
 * @author 张崧
 * @since 2023-12-07 17:06:32
 */
@RestController
@RequestMapping("/admin/erp/warehouseOutItem")
@Api(tags = "出库单商品明细相关API")
@Validated
public class WarehouseOutItemController {

    @Autowired
    private WarehouseOutItemManager warehouseOutItemManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseOutItemVO> list(WarehouseOutItemQueryParams queryParams) {
        return warehouseOutItemManager.list(queryParams);
    }

}

