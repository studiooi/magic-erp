package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.base.CharacterConstant;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import com.hys.app.model.erp.dos.GoodsLabelRelationDO;
import com.hys.app.model.erp.dto.GoodsDTO;
import com.hys.app.model.erp.dto.ProductDTO;
import com.hys.app.model.erp.vo.GoodsVO;
import com.hys.app.model.erp.vo.ProductVO;
import com.hys.app.model.goods.dos.BrandDO;
import com.hys.app.model.goods.dos.GoodsGalleryDO;
import com.hys.app.model.goods.vo.GoodsParamsGroupVO;
import com.hys.app.service.base.SensitiveFilter;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.hys.app.framework.util.CollectionUtils.convertMap;

/**
 * 商品 Convert
 *
 * @author 张崧
 * 2023-12-26 15:56:58
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GoodsConverter {

    GoodsDO convert(GoodsDTO goodsDTO);

    GoodsVO convert(GoodsDO goodsDO);

    List<GoodsVO> convert(List<GoodsDO> list);

    WebPage<GoodsVO> convert(WebPage<GoodsDO> webPage);

    default WebPage<GoodsVO> combination(WebPage<GoodsDO> webPage, Map<Long, String> categoryNameMap, Map<Long, String> brandNameMap, List<GoodsLabelRelationDO> goodsLabelRelationList, List<GoodsLabelDO> goodsLabelList) {
        WebPage<GoodsVO> page = convert(webPage);

        Map<Long, List<GoodsLabelRelationDO>> goodsIdLabelIdsMap = goodsLabelRelationList.stream().collect(Collectors.groupingBy(GoodsLabelRelationDO::getGoodsId));
        Map<Long, String> labelMap = convertMap(goodsLabelList, GoodsLabelDO::getId, GoodsLabelDO::getName);

        for (GoodsVO goodsVO : page.getData()) {
            goodsVO.setCategoryName(categoryNameMap.get(goodsVO.getCategoryId()));
            goodsVO.setBrandName(brandNameMap.get(goodsVO.getBrandId()));
            // 商品标签
            List<GoodsLabelRelationDO> labelRelationList = goodsIdLabelIdsMap.get(goodsVO.getId());
            if (labelRelationList != null) {
                String labelNames = labelRelationList.stream().map(goodsLabelRelationDO -> labelMap.get(goodsLabelRelationDO.getLabelId())).collect(Collectors.joining(","));
                goodsVO.setLabelNames(labelNames);
            }
        }

        return page;
    }

    default GoodsDO combination(GoodsDTO goodsDTO) {
        GoodsDO goodsDO = convert(goodsDTO);

        // 商品名称过滤敏感词
        goodsDO.setName(SensitiveFilter.filter(goodsDTO.getName(), CharacterConstant.WILDCARD_STAR));
        // 商品图片
        goodsDO.setImage(goodsDTO.getImageList().get(0).getThumbnail());
        // 将价格最低的sku的字段初始化到商品上
        ProductDTO minPriceProduct = goodsDTO.getProductList().stream().min(Comparator.comparing(ProductDTO::getPrice)).get();
        goodsDO.setPropertiesFromProduct(minPriceProduct);

        return goodsDO;
    }

    default GoodsVO convert(GoodsDO goodsDO, List<ProductVO> productList, Map catInfo, BrandDO brandDO, List<GoodsGalleryDO> goodsGalleryList, List<GoodsParamsGroupVO> goodsParamsList) {
        GoodsVO goodsVO = convert(goodsDO);

        goodsVO.setProductList(productList);
        goodsVO.setBrandName(brandDO == null ? "" : brandDO.getName());
        goodsVO.setCategoryName(catInfo.get("catName").toString());
        goodsVO.setCategoryIds((Long[]) catInfo.get("catIds"));
        goodsVO.setImageList(goodsGalleryList);
        goodsVO.setParamsList(goodsParamsList);

        return goodsVO;
    }
}

