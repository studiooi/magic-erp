package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import com.hys.app.model.erp.vo.GoodsLabelVO;
import com.hys.app.model.erp.dto.GoodsLabelDTO;
import com.hys.app.model.erp.vo.GoodsLabelExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 商品标签 Converter
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GoodsLabelConverter {
    
    GoodsLabelDO convert(GoodsLabelDTO goodsLabelDTO);
    
    GoodsLabelVO convert(GoodsLabelDO goodsLabelDO);
    
    List<GoodsLabelVO> convertList(List<GoodsLabelDO> list);
    
    List<GoodsLabelExcelVO> convertExcel(List<GoodsLabelVO> list);
    
    WebPage<GoodsLabelVO> convertPage(WebPage<GoodsLabelDO> webPage);
    
}

