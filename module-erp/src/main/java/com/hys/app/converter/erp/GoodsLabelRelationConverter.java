package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.GoodsLabelRelationDO;
import com.hys.app.model.erp.vo.GoodsLabelRelationVO;
import com.hys.app.model.erp.dto.GoodsLabelRelationDTO;
import com.hys.app.model.erp.vo.GoodsLabelRelationExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 商品标签-商品关联表 Converter
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GoodsLabelRelationConverter {
    
    GoodsLabelRelationDO convert(GoodsLabelRelationDTO goodsLabelRelationDTO);
    
    GoodsLabelRelationVO convert(GoodsLabelRelationDO goodsLabelRelationDO);
    
    List<GoodsLabelRelationVO> convertList(List<GoodsLabelRelationDO> list);
    
    List<GoodsLabelRelationExcelVO> convertExcel(List<GoodsLabelRelationVO> list);
    
    WebPage<GoodsLabelRelationVO> convertPage(WebPage<GoodsLabelRelationDO> webPage);
    
}

