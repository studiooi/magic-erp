package com.hys.app.converter.erp;

import com.hys.app.model.erp.dto.MemberExcelImport;
import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.MemberDO;
import com.hys.app.model.erp.vo.MemberVO;
import com.hys.app.model.erp.dto.MemberDTO;
import com.hys.app.model.erp.vo.MemberExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 会员 Converter
 *
 * @author 张崧
 * 2024-01-23 09:28:16
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MemberConverter {
    
    MemberDO convert(MemberDTO memberDTO);
    
    MemberVO convert(MemberDO memberDO);
    
    List<MemberVO> convertList(List<MemberDO> list);
    
    List<MemberExcelVO> convertExcel(List<MemberVO> list);
    
    WebPage<MemberVO> convertPage(WebPage<MemberDO> webPage);

    List<MemberDO> convertImportList(List<MemberExcelImport> importList);
}

