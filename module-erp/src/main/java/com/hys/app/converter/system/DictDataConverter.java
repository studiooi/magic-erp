package com.hys.app.converter.system;

import com.hys.app.model.system.dto.DictDataCreateDTO;
import com.hys.app.model.system.vo.DictDataRespVO;
import com.hys.app.model.system.vo.DictDataSimpleRespVO;
import com.hys.app.model.system.dto.DictDataUpdateDTO;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.DictDataDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DictDataConverter {

    List<DictDataSimpleRespVO> convertList(List<DictDataDO> list);

    DictDataRespVO convert(DictDataDO bean);

    WebPage<DictDataRespVO> convertPage(WebPage<DictDataDO> page);

    DictDataDO convert(DictDataUpdateDTO bean);

    DictDataDO convert(DictDataCreateDTO bean);

}
