package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dos.MemberDO;
import com.hys.app.model.erp.dto.MemberQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 会员 Mapper
 *
 * @author 张崧
 * 2024-01-23 09:28:16
 */
@Mapper
public interface MemberMapper extends BaseMapperX<MemberDO> {

    default WebPage<MemberDO> selectPage(MemberQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(MemberDO::getSn, params.getSn())
                .likeIfPresent(MemberDO::getName, params.getName())
                .eqIfPresent(MemberDO::getSex, params.getSex())
                .likeIfPresent(MemberDO::getMobile, params.getMobile())
                .likeIfPresent(MemberDO::getEmail, params.getEmail())
                .eqIfPresent(MemberDO::getBirthday, params.getBirthday())
                .eqIfPresent(MemberDO::getDisableFlag, params.getDisableFlag())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(MemberDO::getId)
                .page(params);
    }

}

