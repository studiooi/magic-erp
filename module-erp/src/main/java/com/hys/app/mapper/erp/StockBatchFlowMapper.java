package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import com.hys.app.model.erp.dto.StockBatchFlowQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 库存批次流水 Mapper
 *
 * @author 张崧
 * 2024-01-16 11:39:01
 */
@Mapper
public interface StockBatchFlowMapper extends BaseMapperX<StockBatchFlowDO> {

    default WebPage<StockBatchFlowDO> selectPage(StockBatchFlowQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(StockBatchFlowDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .eqIfPresent(StockBatchFlowDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(StockBatchFlowDO::getBatchId, params.getBatchId())
                .eqIfPresent(StockBatchFlowDO::getBatchSn, params.getBatchSn())
                .eqIfPresent(StockBatchFlowDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(StockBatchFlowDO::getGoodsId, params.getGoodsId())
                .eqIfPresent(StockBatchFlowDO::getProductId, params.getProductId())
                .eqIfPresent(StockBatchFlowDO::getProductSn, params.getProductSn())
                .eqIfPresent(StockBatchFlowDO::getProductName, params.getProductName())
                .eqIfPresent(StockBatchFlowDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(StockBatchFlowDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(StockBatchFlowDO::getProductBarcode, params.getProductBarcode())
                .eqIfPresent(StockBatchFlowDO::getCategoryId, params.getCategoryId())
                .eqIfPresent(StockBatchFlowDO::getCategoryName, params.getCategoryName())
                .eqIfPresent(StockBatchFlowDO::getSourceType, params.getSourceType())
                .eqIfPresent(StockBatchFlowDO::getSourceSn, params.getSourceSn())
                .betweenIfPresent(StockBatchFlowDO::getCreateTime, params.getCreateTime())
                .orderByDesc(StockBatchFlowDO::getId)
                .page(params);
    }

}

