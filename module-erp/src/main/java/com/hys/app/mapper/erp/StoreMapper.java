package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.StoreDO;
import com.hys.app.model.erp.dto.StoreQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 门店的Mapper
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
public interface StoreMapper extends BaseMapperX<StoreDO> {

    default WebPage<StoreDO> selectPage(StoreQueryParams params) {
        return lambdaQuery()
                .eq(StoreDO::getDeleteFlag, 0)
                .likeIfPresent(StoreDO::getStoreName, params.getStoreName())
                .eqIfPresent(StoreDO::getAddress, params.getAddress())
                .eqIfPresent(StoreDO::getContactPerson, params.getContactPerson())
                .eqIfPresent(StoreDO::getTelephone, params.getTelephone())
                .eqIfPresent(StoreDO::getCreateTime, params.getCreateTime())
                .orderByDesc(StoreDO::getId)
                .page(params);
    }

}

