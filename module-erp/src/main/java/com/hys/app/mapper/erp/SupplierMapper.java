package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.SupplierDO;
import com.hys.app.model.erp.dto.SupplierQueryParams;
import com.hys.app.framework.database.WebPage;

import java.util.List;

/**
 * 供应商的Mapper
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
public interface SupplierMapper extends BaseMapperX<SupplierDO> {

    default WebPage<SupplierDO> selectPage(SupplierQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(SupplierDO::getCustomName, params.getKeyword())
                .likeIfPresent(SupplierDO::getCustomName, params.getCustomName())
                .likeIfPresent(SupplierDO::getCustomSn, params.getCustomSn())
                .likeIfPresent(SupplierDO::getTransfer, params.getTransfer())
                .likeIfPresent(SupplierDO::getZipcode, params.getZipcode())
                .likeIfPresent(SupplierDO::getPostalAddress, params.getPostalAddress())
                .likeIfPresent(SupplierDO::getTelephone, params.getTelephone())
                .likeIfPresent(SupplierDO::getCompanyWebsite, params.getCompanyWebsite())
                .likeIfPresent(SupplierDO::getLinkman, params.getLinkman())
                .likeIfPresent(SupplierDO::getEmail, params.getEmail())
                .likeIfPresent(SupplierDO::getMobile, params.getMobile())
                .likeIfPresent(SupplierDO::getProductionAddress, params.getProductionAddress())
                .likeIfPresent(SupplierDO::getRemark, params.getRemark())
                .orderByDesc(SupplierDO::getId)
                .page(params);
    }

    default List<SupplierDO> selectNormalStatusList(){
        return lambdaQuery()
                .eq(SupplierDO::getDisableFlag, false)
                .orderByDesc(SupplierDO::getId)
                .list();
    }

    default void updateDisable(Long id, Boolean disable){
        lambdaUpdate().set(SupplierDO::getDisableFlag, disable).eq(SupplierDO::getId, id).update();
    }
}

