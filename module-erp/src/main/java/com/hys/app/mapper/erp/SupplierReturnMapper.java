package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.SupplierReturnDO;
import com.hys.app.model.erp.dto.SupplierReturnQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.SupplierReturnStatisticsParam;
import com.hys.app.model.erp.vo.SupplierReturnStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 供应商退货的Mapper
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
public interface SupplierReturnMapper extends BaseMapperX<SupplierReturnDO> {

    default WebPage<SupplierReturnDO> selectPage(SupplierReturnQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(SupplierReturnDO::getSn, params.getSn())
                .eqIfPresent(SupplierReturnDO::getDeptId, params.getDeptId())
                .eqIfPresent(SupplierReturnDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .likeIfPresent(SupplierReturnDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(SupplierReturnDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(SupplierReturnDO::getSupplierId, params.getSupplierId())
                .eqIfPresent(SupplierReturnDO::getContractId, params.getContractId())
                .likeIfPresent(SupplierReturnDO::getHandleByName, params.getHandleBy())
                .eqIfPresent(SupplierReturnDO::getAttachment, params.getAttachment())
                .eqIfPresent(SupplierReturnDO::getReturnTime, params.getReturnTime())
                .eqIfPresent(SupplierReturnDO::getStatus, params.getStatus())
                .eqIfPresent(SupplierReturnDO::getAuditRemark, params.getAuditRemark())
                .geIfPresent(SupplierReturnDO::getReturnTime, params.getStartTime())
                .leIfPresent(SupplierReturnDO::getReturnTime, params.getEndTime())
                .orderByDesc(SupplierReturnDO::getReturnTime)
                .page(params);
    }

    /**
     * 查询供应商退货统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<SupplierReturnStatistics> selectSupplierReturnPage(Page page, @Param("param") SupplierReturnStatisticsParam param);

    /**
     * 导出供应商退货统计列表
     *
     * @param param 查询参数
     * @return
     */
    List<SupplierReturnStatistics> selectSupplierReturnList(@Param("param") SupplierReturnStatisticsParam param);
}

