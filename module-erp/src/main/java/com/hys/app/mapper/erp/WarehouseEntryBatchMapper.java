package com.hys.app.mapper.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dto.WarehouseEntryBatchQueryParams;

import java.util.List;

/**
 * 入库批次的Mapper
 *
 * @author 张崧
 * @since 2023-12-08 11:49:52
 */
public interface WarehouseEntryBatchMapper extends BaseMapperX<WarehouseEntryBatchDO> {

    default WebPage<WarehouseEntryBatchDO> selectPage(WarehouseEntryBatchQueryParams params) {
        return lambdaQuery()
                .andX(StringUtil.notEmpty(params.getKeyword()),
                        w -> w.like(WarehouseEntryBatchDO::getProductName, params.getKeyword()).or()
                                .like(WarehouseEntryBatchDO::getProductSn, params.getKeyword()))
                .eqIfPresent(WarehouseEntryBatchDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .likeIfPresent(WarehouseEntryBatchDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .likeIfPresent(WarehouseEntryBatchDO::getSn, params.getSn())
                .eqIfPresent(WarehouseEntryBatchDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(WarehouseEntryBatchDO::getProductId, params.getProductId())
                .orderByDesc(WarehouseEntryBatchDO::getId)
                .page(params);
    }

    default List<WarehouseEntryBatchDO> listAll(WarehouseEntryBatchQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(WarehouseEntryBatchDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .likeIfPresent(WarehouseEntryBatchDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .likeIfPresent(WarehouseEntryBatchDO::getSn, params.getSn())
                .eqIfPresent(WarehouseEntryBatchDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(WarehouseEntryBatchDO::getProductId, params.getProductId())
                .orderByDesc(WarehouseEntryBatchDO::getId)
                .list();
    }
}

