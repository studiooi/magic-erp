package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryProductQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 入库单商品明细的Mapper
 *
 * @author 张崧
 * @since 2023-12-05 15:30:10
 */
public interface WarehouseEntryProductMapper extends BaseMapperX<WarehouseEntryProductDO> {

    default WebPage<WarehouseEntryProductDO> selectPage(WarehouseEntryProductQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(WarehouseEntryProductDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .likeIfPresent(WarehouseEntryProductDO::getSn, params.getSn())
                .likeIfPresent(WarehouseEntryProductDO::getName, params.getName())
                .likeIfPresent(WarehouseEntryProductDO::getSpecification, params.getSpecification())
                .likeIfPresent(WarehouseEntryProductDO::getBrand, params.getBrand())
                .likeIfPresent(WarehouseEntryProductDO::getUnit, params.getUnit())
                .eqIfPresent(WarehouseEntryProductDO::getContractNum, params.getContractNum())
                .eqIfPresent(WarehouseEntryProductDO::getNum, params.getNum())
                .orderByDesc(WarehouseEntryProductDO::getId)
                .page(params);
    }

}

