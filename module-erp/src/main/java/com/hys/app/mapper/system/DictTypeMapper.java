package com.hys.app.mapper.system;

import com.hys.app.model.system.dto.DictTypeQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.DictTypeDO;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.wrapper.LambdaQueryWrapperX;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface DictTypeMapper extends BaseMapperX<DictTypeDO> {

    default WebPage<DictTypeDO> selectPage(DictTypeQueryParams reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<DictTypeDO>()
                .likeIfPresent(DictTypeDO::getName, reqVO.getName())
                .likeIfPresent(DictTypeDO::getType, reqVO.getType())
                .orderByDesc(DictTypeDO::getId));
    }

    default DictTypeDO selectByType(String type) {
        return lambdaQuery().eq(DictTypeDO::getType, type).one();
    }

    default DictTypeDO selectByName(String name) {
        return lambdaQuery().eq(DictTypeDO::getName, name).one();
    }

    default void updateToDelete(Long id, Long deletedTime) {
        lambdaUpdate()
                .set(DictTypeDO::getDeleted, true)
                .set(DictTypeDO::getDeletedTime, deletedTime)
                .eq(DictTypeDO::getId, id)
                .update();
    }
}
