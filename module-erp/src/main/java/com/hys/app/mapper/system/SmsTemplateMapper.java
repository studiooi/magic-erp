package com.hys.app.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.SmsTemplate;
import org.apache.ibatis.annotations.CacheNamespace;


/**
 * 短信模板Mapper
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:22
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction= MybatisRedisCache.class)
public interface SmsTemplateMapper extends BaseMapper<SmsTemplate> {


}