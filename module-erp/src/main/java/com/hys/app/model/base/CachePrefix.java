package com.hys.app.model.base;

/**
 * 缓存前缀
 * Created by kingapex on 2018/3/19.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/19
 */
public enum CachePrefix {

    /**
     * 系统设置
     */
    SETTING,

    /**
     * 快递平台
     */
    EXPRESS,

    /**
     * 快递平台
     */
    SHIP_TEMPLATE,

    /**
     * 图片验证码
     */
    CAPTCHA,

    /**
     * 商品分类
     */
    GOODS_CAT,
    /**
     * 存储方案
     */
    UPLOADER,

    /**
     * 短信网关
     */
    SPlATFORM,
    /**
     * smtp
     */
    SMTP,
    /**
     * 短信验证码
     */
    SMS_CODE,
    /**
     * 邮箱验证码
     */
    EMAIL_CODE,
    /**
     * 管理员角色权限对照表
     */
    ADMIN_URL_ROLE,

    /**
     * 手机验证标识
     */
    MOBILE_VALIDATE,

    /**
     * 邮箱验证标识
     */
    EMAIL_VALIDATE,

    /**
     * 店铺中某个运费模版
     */
    SHIP_TEMPLATE_ONE,

    /**
     * 所有地区
     */
    REGIONALL,

    /**
     * 分级别地区缓存
     */
    REGIONLIDEPTH,

    /**
     * 验证平台
     */
    VALIDATOR_PLATFORM,

    /**
     * 手机号码发送短信次数
     */
    PHONE_SMS_NUMBER,

    /**
     * 部门缓存（数据权限使用）
     */
    DEPT_CHILDREN_ID_LIST,

    /**
     * 用友token
     */
    VOUCHER_TOKEN,
    /**
     * 用友凭证app ticket
     */
    VOUCHER_APP_TICKET,
    ;

    public String getPrefix() {
        return "{" + this.name() + "}_";
    }
}
