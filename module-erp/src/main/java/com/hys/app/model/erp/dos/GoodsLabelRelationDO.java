package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品标签-商品关联表 DO
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@TableName("erp_goods_label_relation")
@Data
public class GoodsLabelRelationDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "label_id", value = "标签id")
    private Long labelId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

}
