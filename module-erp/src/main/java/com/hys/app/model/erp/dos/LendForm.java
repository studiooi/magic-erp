package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.vo.LendFormAllowable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 商品借出单实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_lend_form")
@ApiModel
public class LendForm implements Serializable {

    private static final long serialVersionUID = -7101067257841558935L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 状态
     * @see com.hys.app.model.erp.enums.LendFormStatusEnum
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    /**
     * 借出人ID
     */
    @ApiModelProperty(name = "lend_person_id", value = "借出人ID")
    private Long lendPersonId;
    /**
     * 借出人
     */
    @ApiModelProperty(name = "lend_person", value = "借出人")
    private String lendPerson;
    /**
     * 借出登记人ID
     */
    @ApiModelProperty(name = "lend_register_id", value = "借出登记人ID")
    private Long lendRegisterId;
    /**
     * 借出登记人
     */
    @ApiModelProperty(name = "lend_register", value = "借出登记人")
    private String lendRegister;
    /**
     * 借出时间
     */
    @ApiModelProperty(name = "lend_time", value = "借出时间")
    private Long lendTime;
    /**
     * 借出说明
     */
    @ApiModelProperty(name = "lend_desc", value = "借出说明")
    private String lendDesc;
    /**
     * 归还人ID
     */
    @ApiModelProperty(name = "return_person_id", value = "归还人ID")
    private Long returnPersonId;
    /**
     * 归还人
     */
    @ApiModelProperty(name = "return_person", value = "归还人")
    private String returnPerson;
    /**
     * 归还登记人ID
     */
    @ApiModelProperty(name = "return_register_id", value = "归还登记人ID")
    private Long returnRegisterId;
    /**
     * 归还登记人
     */
    @ApiModelProperty(name = "return_register", value = "归还登记人")
    private String returnRegister;
    /**
     * 归还时间
     */
    @ApiModelProperty(name = "return_time", value = "归还时间")
    private Long returnTime;
    /**
     * 归还说明
     */
    @ApiModelProperty(name = "return_desc", value = "归还说明")
    private String returnDesc;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 确认时间
     */
    @ApiModelProperty(name = "confirm_time", value = "确认时间")
    private Long confirmTime;
    /**
     * 借出单允许进行的操作
     */
    @TableField(exist = false)
    @ApiModelProperty(name = "allowable", value = "借出单允许进行的操作")
    private LendFormAllowable allowable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getLendPersonId() {
        return lendPersonId;
    }

    public void setLendPersonId(Long lendPersonId) {
        this.lendPersonId = lendPersonId;
    }

    public String getLendPerson() {
        return lendPerson;
    }

    public void setLendPerson(String lendPerson) {
        this.lendPerson = lendPerson;
    }

    public Long getLendRegisterId() {
        return lendRegisterId;
    }

    public void setLendRegisterId(Long lendRegisterId) {
        this.lendRegisterId = lendRegisterId;
    }

    public String getLendRegister() {
        return lendRegister;
    }

    public void setLendRegister(String lendRegister) {
        this.lendRegister = lendRegister;
    }

    public Long getLendTime() {
        return lendTime;
    }

    public void setLendTime(Long lendTime) {
        this.lendTime = lendTime;
    }

    public String getLendDesc() {
        return lendDesc;
    }

    public void setLendDesc(String lendDesc) {
        this.lendDesc = lendDesc;
    }

    public Long getReturnPersonId() {
        return returnPersonId;
    }

    public void setReturnPersonId(Long returnPersonId) {
        this.returnPersonId = returnPersonId;
    }

    public String getReturnPerson() {
        return returnPerson;
    }

    public void setReturnPerson(String returnPerson) {
        this.returnPerson = returnPerson;
    }

    public Long getReturnRegisterId() {
        return returnRegisterId;
    }

    public void setReturnRegisterId(Long returnRegisterId) {
        this.returnRegisterId = returnRegisterId;
    }

    public String getReturnRegister() {
        return returnRegister;
    }

    public void setReturnRegister(String returnRegister) {
        this.returnRegister = returnRegister;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Long returnTime) {
        this.returnTime = returnTime;
    }

    public String getReturnDesc() {
        return returnDesc;
    }

    public void setReturnDesc(String returnDesc) {
        this.returnDesc = returnDesc;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Long confirmTime) {
        this.confirmTime = confirmTime;
    }

    public LendFormAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(LendFormAllowable allowable) {
        this.allowable = allowable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LendForm lendForm = (LendForm) o;
        return Objects.equals(id, lendForm.id) &&
                Objects.equals(sn, lendForm.sn) &&
                Objects.equals(status, lendForm.status) &&
                Objects.equals(deptId, lendForm.deptId) &&
                Objects.equals(deptName, lendForm.deptName) &&
                Objects.equals(warehouseId, lendForm.warehouseId) &&
                Objects.equals(warehouseName, lendForm.warehouseName) &&
                Objects.equals(lendPersonId, lendForm.lendPersonId) &&
                Objects.equals(lendPerson, lendForm.lendPerson) &&
                Objects.equals(lendRegisterId, lendForm.lendRegisterId) &&
                Objects.equals(lendRegister, lendForm.lendRegister) &&
                Objects.equals(lendTime, lendForm.lendTime) &&
                Objects.equals(lendDesc, lendForm.lendDesc) &&
                Objects.equals(returnPersonId, lendForm.returnPersonId) &&
                Objects.equals(returnPerson, lendForm.returnPerson) &&
                Objects.equals(returnRegisterId, lendForm.returnRegisterId) &&
                Objects.equals(returnRegister, lendForm.returnRegister) &&
                Objects.equals(returnTime, lendForm.returnTime) &&
                Objects.equals(returnDesc, lendForm.returnDesc) &&
                Objects.equals(createTime, lendForm.createTime) &&
                Objects.equals(confirmTime, lendForm.confirmTime) &&
                Objects.equals(allowable, lendForm.allowable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, status, deptId, deptName, warehouseId, warehouseName, lendPersonId, lendPerson, lendRegisterId, lendRegister, lendTime, lendDesc, returnPersonId, returnPerson, returnRegisterId, returnRegister, returnTime, returnDesc, createTime, confirmTime, allowable);
    }

    @Override
    public String toString() {
        return "LendForm{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", status='" + status + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", lendPersonId=" + lendPersonId +
                ", lendPerson='" + lendPerson + '\'' +
                ", lendRegisterId=" + lendRegisterId +
                ", lendRegister='" + lendRegister + '\'' +
                ", lendTime=" + lendTime +
                ", lendDesc='" + lendDesc + '\'' +
                ", returnPersonId=" + returnPersonId +
                ", returnPerson='" + returnPerson + '\'' +
                ", returnRegisterId=" + returnRegisterId +
                ", returnRegister='" + returnRegister + '\'' +
                ", returnTime=" + returnTime +
                ", returnDesc='" + returnDesc + '\'' +
                ", createTime=" + createTime +
                ", confirmTime=" + confirmTime +
                ", allowable=" + allowable +
                '}';
    }
}
