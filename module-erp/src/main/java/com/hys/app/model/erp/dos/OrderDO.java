package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.enums.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 订单 DO
 *
 * @author 张崧
 * 2024-01-24 15:58:30
 */
@TableName(value = "erp_order", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    // ================== 基本信息 ==================

    @ApiModelProperty(name = "sn", value = "订单编号")
    private String sn;

    @ApiModelProperty(name = "status", value = "订单状态")
    private OrderStatusEnum status;

    @ApiModelProperty(name = "payment_status", value = "支付状态")
    private OrderPaymentStatusEnum paymentStatus;

    @ApiModelProperty(name = "payment_time", value = "支付时间")
    private Long paymentTime;

    @ApiModelProperty(name = "owner_id", value = "会员id/企业id/第三方用户id")
    private String ownerId;

    @ApiModelProperty(name = "owner_name", value = "会员名称/企业名称/第三方用户名称")
    private String ownerName;

    @ApiModelProperty(name = "owner_mobile", value = "会员手机号/企业手机号/第三方用户手机号")
    private String ownerMobile;

    @ApiModelProperty(name = "order_time", value = "下单时间")
    private Long orderTime;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "marketing_id", value = "销售经理id")
    private Long marketingId;

    @ApiModelProperty(name = "marketing_name", value = "销售经理名称")
    private String marketingName;

    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    @ApiModelProperty(name = "type", value = "订单类型")
    private OrderTypeEnum type;

    @ApiModelProperty(name = "source", value = "订单来源")
    private OrderSourceEnum source;

    @ApiModelProperty(name = "source_id", value = "订单来源id")
    private String sourceId;

    // ================== 价格相关 ==================

    @ApiModelProperty(name = "total_price", value = "销售金额")
    private Double totalPrice;

    @ApiModelProperty(name = "discount_price", value = "优惠金额")
    private Double discountPrice;

    @ApiModelProperty(name = "tax_price", value = "税额")
    private Double taxPrice;

    @ApiModelProperty(name = "pay_price", value = "应付金额")
    private Double payPrice;

    @ApiModelProperty(name = "deposit_price", value = "定金金额")
    private Double depositPrice;

    // ================== 配送相关 ==================

    @ApiModelProperty(name = "delivery_type", value = "配送方式")
    private OrderDeliveryType deliveryType;

    @ApiModelProperty(name = "ship_region_ids", value = "物流配送地区id集合")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Long> shipRegionIds;

    @ApiModelProperty(name = "ship_region_names", value = "物流配送地区名称集合")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> shipRegionNames;

    @ApiModelProperty(name = "ship_address", value = "物流配送详细地址")
    private String shipAddress;

    @ApiModelProperty(name = "store_id", value = "自提门店id")
    private Long storeId;

    @ApiModelProperty(name = "store_name", value = "自提门店名称")
    private String storeName;

    @ApiModelProperty(name = "warehouse_out_flag", value = "是否已出库")
    private Boolean warehouseOutFlag;

    @ApiModelProperty(name = "warehouse_out_id", value = "出库单id")
    private Long warehouseOutId;

    @ApiModelProperty(name = "ship_flag", value = "是否已发货")
    private Boolean shipFlag;

    @ApiModelProperty(name = "ship_time", value = "发货时间")
    private Long shipTime;

    @ApiModelProperty(name = "logistics_company_id", value = "物流公司id")
    private Long logisticsCompanyId;

    @ApiModelProperty(name = "logistics_company_name", value = "物流公司名称")
    private String logisticsCompanyName;

    @ApiModelProperty(name = "logistics_tracking_number", value = "物流单号")
    private String logisticsTrackingNumber;

    @ApiModelProperty(name = "deleted", value = "是否删除")
    @TableLogic
    private Boolean deleted;

}
