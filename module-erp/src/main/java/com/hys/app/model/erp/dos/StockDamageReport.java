package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.enums.StockDamageReportTypeEnum;
import com.hys.app.model.erp.vo.StockDamageReportAllowable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 库存报损单实体
 *
 * @author dmy
 * 2023-12-05
 */
@TableName("es_stock_damage_report")
@ApiModel
public class StockDamageReport implements Serializable {

    private static final long serialVersionUID = -2812668888351646009L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人")
    private String creator;
    /**
     * 报损时间
     */
    @ApiModelProperty(name = "report_time", value = "报损时间")
    private Long reportTime;
    /**
     * 报损类型（键）
     */
    @ApiModelProperty(name = "damage_type_key", value = "报损类型（键）")
    private String damageTypeKey;
    /**
     * 报损类型（值） {@link StockDamageReportTypeEnum}
     */
    @ApiModelProperty(name = "damage_type_value", value = "报损类型（值）")
    private String damageTypeValue;
    /**
     * 报损说明
     */
    @ApiModelProperty(name = "report_desc", value = "报损说明")
    private String reportDesc;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 状态
     * @see com.hys.app.model.erp.enums.StockDamageReportStatusEnum
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    /**
     * 审核驳回原因
     */
    @ApiModelProperty(name = "reject_reason", value = "审核驳回原因")
    private String rejectReason;
    /**
     * 审核时间
     */
    @ApiModelProperty(name = "audit_time", value = "审核时间")
    private Long auditTime;
    /**
     * 库存报损单允许进行的操作
     */
    @TableField(exist = false)
    @ApiModelProperty(name = "allowable", value = "库存报损单允许进行的操作")
    private StockDamageReportAllowable allowable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getReportTime() {
        return reportTime;
    }

    public void setReportTime(Long reportTime) {
        this.reportTime = reportTime;
    }

    public String getDamageTypeKey() {
        return damageTypeKey;
    }

    public void setDamageTypeKey(String damageTypeKey) {
        this.damageTypeKey = damageTypeKey;
    }

    public String getDamageTypeValue() {
        return damageTypeValue;
    }

    public void setDamageTypeValue(String damageTypeValue) {
        this.damageTypeValue = damageTypeValue;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Long getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Long auditTime) {
        this.auditTime = auditTime;
    }

    public StockDamageReportAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(StockDamageReportAllowable allowable) {
        this.allowable = allowable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockDamageReport that = (StockDamageReport) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sn, that.sn) &&
                Objects.equals(deptId, that.deptId) &&
                Objects.equals(deptName, that.deptName) &&
                Objects.equals(warehouseId, that.warehouseId) &&
                Objects.equals(warehouseName, that.warehouseName) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(creator, that.creator) &&
                Objects.equals(reportTime, that.reportTime) &&
                Objects.equals(damageTypeKey, that.damageTypeKey) &&
                Objects.equals(damageTypeValue, that.damageTypeValue) &&
                Objects.equals(reportDesc, that.reportDesc) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(status, that.status) &&
                Objects.equals(rejectReason, that.rejectReason) &&
                Objects.equals(auditTime, that.auditTime) &&
                Objects.equals(allowable, that.allowable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, deptId, deptName, warehouseId, warehouseName, creatorId, creator, reportTime, damageTypeKey, damageTypeValue, reportDesc, createTime, status, rejectReason, auditTime, allowable);
    }

    @Override
    public String toString() {
        return "StockDamageReport{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", reportTime=" + reportTime +
                ", damageTypeKey='" + damageTypeKey + '\'' +
                ", damageTypeValue='" + damageTypeValue + '\'' +
                ", reportDesc='" + reportDesc + '\'' +
                ", createTime=" + createTime +
                ", status='" + status + '\'' +
                ", rejectReason='" + rejectReason + '\'' +
                ", auditTime=" + auditTime +
                ", allowable=" + allowable +
                '}';
    }
}
