package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.enums.StockTransferStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 库存调拨实体类
 *
 * @author 张崧
 * @since 2023-12-12 11:59:06
 */
@TableName("erp_stock_transfer")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StockTransferDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;

    @ApiModelProperty(name = "status", value = "状态")
    private StockTransferStatusEnum status;

    @ApiModelProperty(name = "out_dept_id", value = "调出方部门id")
    private Long outDeptId;

    @ApiModelProperty(name = "out_warehouse_id", value = "调出方仓库id")
    private Long outWarehouseId;

    @ApiModelProperty(name = "out_handled_by_id", value = "调出方仓库经手人id")
    private Long outHandledById;

    @ApiModelProperty(name = "out_handled_by_name", value = "调出方仓库经手人名称")
    private String outHandledByName;

    @ApiModelProperty(name = "in_dept_id", value = "调入方部门id")
    private Long inDeptId;

    @ApiModelProperty(name = "in_warehouse_id", value = "调入方仓库id")
    private Long inWarehouseId;

    @ApiModelProperty(name = "in_handled_by_id", value = "调入方仓库经手人id")
    private Long inHandledById;

    @ApiModelProperty(name = "in_handled_by_name", value = "调入方仓库经手人名称")
    private String inHandledByName;

    @ApiModelProperty(name = "out_dept_name", value = "调出部门名称")
    private String outDeptName;

    @ApiModelProperty(name = "out_warehouse_name", value = "调出仓库名称")
    private String outWarehouseName;

    @ApiModelProperty(name = "in_dept_name", value = "调入部门名称")
    private String inDeptName;

    @ApiModelProperty(name = "in_warehouse_name", value = "调入仓库名称")
    private String inWarehouseName;

    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;

    @ApiModelProperty(name = "transfer_time", value = "调拨时间")
    private Long transferTime;

}
