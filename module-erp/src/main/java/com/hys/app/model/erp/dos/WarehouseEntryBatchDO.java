package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.enums.StockChangeSourceEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;


/**
 * 入库批次实体类
 *
 * @author 张崧
 * @since 2023-12-08 11:49:52
 */
@TableName("erp_warehouse_entry_batch")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryBatchDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;

    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;

    @ApiModelProperty(name = "warehouse_entry_item_id", value = "入库单明细id")
    private Long warehouseEntryItemId;

    @ApiModelProperty(name = "sn", value = "批次编号")
    private String sn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;

    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;

    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;

    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;
    
    @ApiModelProperty(name = "entry_num", value = "入库数量")
    private Integer entryNum;
    
    @ApiModelProperty(name = "entry_price", value = "入库单价")
    private Double entryPrice;

    @ApiModelProperty(name = "product_cost_price", value = "进货单价")
    private Double productCostPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;
    
    @ApiModelProperty(name = "remain_num", value = "剩余数量")
    private Integer remainNum;
    
    @ApiModelProperty(name = "entry_time", value = "入库时间")
    private Long entryTime;

    @ApiModelProperty(name = "create_source", value = "批次创建来源")
    private StockChangeSourceEnum createSource;
}
