package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 营销经理查询参数
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MarketingManagerQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字搜索")
    private String keyword;

    @ApiModelProperty(name = "disable_flag", value = "0正常 1禁用")
    private Integer disableFlag;
    
    @ApiModelProperty(name = "real_name", value = "真实姓名")
    private String realName;
    
    @ApiModelProperty(name = "mobile", value = "电话")
    private String mobile;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
}

