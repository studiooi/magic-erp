package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 订单明细 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-01-24 16:39:38
 */
@Data
@ApiModel(value = "订单明细 新增|编辑 请求参数")
public class OrderItemDTO {

    @ApiModelProperty(name = "product_id", value = "产品id（订单为ToB时传递）")
    private Long productId;

    @ApiModelProperty(name = "batch_id", value = "库存批次id（订单为ToC时传递）")
    private Long batchId;

    @ApiModelProperty(name = "num", value = "数量")
    @NotNull(message = "数量不能为空")
    @Min(value = 1, message = "数量必须大于0")
    private Integer num;

    @ApiModelProperty(name = "price", value = "销售价（单）")
    @NotNull(message = "销售价（单）不能为空")
    @Min(value = 0, message = "销售价（单）不能小于0")
    private Double price;

    @ApiModelProperty(name = "total_price", value = "销售价（总）")
    @NotNull(message = "销售价（总）不能为空")
    @Min(value = 0, message = "销售价（总）不能小于0")
    private Double totalPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    @NotNull(message = "税率不能为空")
    @Min(value = 0, message = "税率不能小于0")
    private Double taxRate;

    @ApiModelProperty(name = "tax_price", value = "税额（总）")
    @NotNull(message = "税额（总）不能为空")
    @Min(value = 0, message = "税额（总）不能小于0")
    private Double taxPrice;

    @ApiModelProperty(name = "pay_price", value = "应付金额（总）")
    @NotNull(message = "应付金额（总）不能为空")
    @Min(value = 0, message = "应付金额（总）不能小于0")
    private Double payPrice;

    @ApiModelProperty(name = "discount_price", value = "优惠金额（总）")
    @NotNull(message = "优惠金额不能为空")
    @Min(value = 0, message = "优惠金额不能小于0")
    private Double discountPrice;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}

