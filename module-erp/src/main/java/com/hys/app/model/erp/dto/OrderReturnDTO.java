package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.*;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.dos.DeptDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 订单退货新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Data
public class OrderReturnDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "order_id", value = "要退货的订单")
    @NotNull(message = "要退货的订单不能为空")
    private Long orderId;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    @NotNull(message = "部门id不能为空")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    @NotNull(message = "仓库id不能为空")
    private Long warehouseId;
    
    @ApiModelProperty(name = "handle_by_id", value = "经手人id")
    @NotNull(message = "经手人id不能为空")
    private Long handleById;

    @ApiModelProperty(name = "return_time", value = "退货时间")
    @NotNull(message = "退货时间不能为空")
    private Long returnTime;

    @ApiModelProperty(name = "return_remark", value = "退货说明")
    @NotBlank(message = "退货说明不能为空")
    private String returnRemark;

    @ApiModelProperty(name = "item_list", value = "退货项列表")
    @NotEmpty(message = "退货项列表不能为空")
    private List<OrderReturnItemDTO> itemList;


    @JsonIgnore
    private OrderDO orderDO;

    @JsonIgnore
    private Map<Long, WarehouseOutItemDO> warehouseOutItemMap;

    @JsonIgnore
    private Map<Long, OrderItemDO> orderItemMap;

    @JsonIgnore
    private Map<Long, WarehouseEntryBatchDO> batchMap;

    @JsonIgnore
    private DeptDO deptDO;

    @JsonIgnore
    private WarehouseDO warehouseDO;

    @JsonIgnore
    private AdminUser handleBy;

}

