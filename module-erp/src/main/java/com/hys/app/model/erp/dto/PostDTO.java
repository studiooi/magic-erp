package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 岗位新增/编辑DTO
 *
 * @author 张崧
 * 2023-11-29 17:15:22
 */
@Data
public class PostDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "岗位名称")
    @NotBlank(message = "岗位名称不能为空")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "岗位编号")
    @NotBlank(message = "岗位编号不能为空")
    private String sn;
    
    @ApiModelProperty(name = "sort", value = "顺序")
    @NotNull(message = "顺序不能为空")
    private Integer sort;
    
    @ApiModelProperty(name = "description", value = "岗位描述")
    private String description;
    
    @ApiModelProperty(name = "members", value = "岗位成员")
    private String members;
    
}

