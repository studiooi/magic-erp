package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 采购合同查询参数实体
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementContractQueryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = -1674371675460971122L;

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "sn", value = "合同编号")
    private String sn;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "creator_id", value = "制单人ID")
    private Long creatorId;

    /**
     * 合同状态
     * @see com.hys.app.model.erp.enums.ContractStatusEnum
     */
    @ApiModelProperty(name = "contract_status", value = "合同状态")
    private String contractStatus;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    @Override
    public String toString() {
        return "ProcurementContractQueryParam{" +
                "keyword='" + keyword + '\'' +
                ", sn='" + sn + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", creatorId=" + creatorId +
                '}';
    }
}
