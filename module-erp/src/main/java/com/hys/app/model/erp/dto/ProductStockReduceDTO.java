package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 扣减商品库存
 *
 * @author 张崧
 * @since 2023-12-07
 */
@Data
public class ProductStockReduceDTO {

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;

    @ApiModelProperty(name = "change_num", value = "要扣减的库存数量，必须大于0")
    private Integer changeNum;

    @ApiModelProperty(name = "batch_id", value = "批次id")
    private Long batchId;

    @ApiModelProperty(name = "batch_sn", value = "批次sn(目前扣减批次是根据批次id扣减的，如果扣减失败该字段用于展示用)")
    private String batchSn;
}

