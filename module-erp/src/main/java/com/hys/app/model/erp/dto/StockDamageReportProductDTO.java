package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 库存报损单产品实体DTO
 *
 * @author dmy
 * 2023-12-05
 */
public class StockDamageReportProductDTO implements Serializable {

    private static final long serialVersionUID = -1175913255503592167L;

    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号", required = true)
    @NotEmpty(message = "入库单编号不能为空")
    private String stockSn;
    /**
     * 批次ID
     */
    @ApiModelProperty(name = "batch_id", value = "批次ID", required = true)
    @NotNull(message = "批次ID不能为空")
    private Long batchId;
    /**
     * 批次编号
     */
    @ApiModelProperty(name = "batch_sn", value = "批次编号", required = true)
    @NotEmpty(message = "批次编号不能为空")
    private String batchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID", required = true)
    @NotNull(message = "商品ID不能为空")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称", required = true)
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号", required = true)
    @NotEmpty(message = "产品编号不能为空")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格", required = true)
    @NotEmpty(message = "产品规格不能为空")
    private String specification;
    /**
     * 分类id
     */
    @ApiModelProperty(name = "category_id", value = "分类id", required = true)
    @NotNull(message = "分类id不能为空")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = true)
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位", required = true)
    private String unit;
    /**
     * 单价
     */
    @ApiModelProperty(name = "price", value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    private Double price;
    /**
     * 成本价
     */
    @ApiModelProperty(name = "cost_price", value = "成本价")
    @NotNull(message = "成本价不能为空")
    private Double costPrice;
    /**
     * 税率
     */
    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量", required = true)
    @NotNull(message = "库存数量不能为空")
    private Integer stockNum;
    /**
     * 报损数量
     */
    @ApiModelProperty(name = "report_num", value = "报损数量", required = true)
    @NotNull(message = "报损数量不能为空")
    private Integer reportNum;
    /**
     * 备注
     */
    @ApiModelProperty(name = "report_remark", value = "备注")
    private String reportRemark;
    /**
     * 类型  0：增加，1：减少
     */
    @ApiModelProperty(name = "type", value = "类型 0：增加，1：减少", required = true, allowableValues = "0,1")
    @NotNull(message = "类型不能为空")
    private Integer type;

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getBatchSn() {
        return batchSn;
    }

    public void setBatchSn(String batchSn) {
        this.batchSn = batchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getReportNum() {
        return reportNum;
    }

    public void setReportNum(Integer reportNum) {
        this.reportNum = reportNum;
    }

    public String getReportRemark() {
        return reportRemark;
    }

    public void setReportRemark(String reportRemark) {
        this.reportRemark = reportRemark;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "StockDamageReportProductDTO{" +
                "stockSn='" + stockSn + '\'' +
                ", batchId=" + batchId +
                ", batchSn='" + batchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", costPrice=" + costPrice +
                ", taxRate=" + taxRate +
                ", stockNum=" + stockNum +
                ", reportNum=" + reportNum +
                ", reportRemark='" + reportRemark + '\'' +
                ", type=" + type +
                '}';
    }
}
