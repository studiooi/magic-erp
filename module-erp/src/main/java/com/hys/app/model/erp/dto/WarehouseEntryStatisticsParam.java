package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 入库单统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class WarehouseEntryStatisticsParam extends WarehouseEntryQueryParams implements Serializable {

    private static final long serialVersionUID = 8104176452314372175L;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Override
    public String toString() {
        return "WarehouseEntryStatisticsParam{" +
                "categoryId=" + categoryId +
                ", productName='" + productName + '\'' +
                ", supplierName='" + supplierName + '\'' +
                '}';
    }
}
