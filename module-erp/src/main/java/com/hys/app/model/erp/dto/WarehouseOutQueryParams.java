package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 出库单查询参数
 *
 * @author 张崧
 * @since 2023-12-07 16:50:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "出库单编号")
    private String sn;
    
    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;

    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;

}

