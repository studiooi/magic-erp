package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.OrderReturnDO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 订单退货审核通过消息
 *
 * @author 张崧
 * @since 2024-01-09
 */
@Data
public class OrderReturnAuditPassMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    private List<OrderReturnDO> list;

}
