package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单配送方式
 *
 * @author 张崧
 * @since 2023-12-11
 */
@Getter
@AllArgsConstructor
public enum OrderDeliveryType {

    /**
     * 快递配送
     */
    express("快递配送"),

    /**
     * 门店自提
     */
    self_pick("门店自提");

    private final String text;
}
