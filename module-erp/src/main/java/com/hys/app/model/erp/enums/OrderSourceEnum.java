package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单来源枚举
 *
 * @author 张崧
 * @since 2024-03-29
 */
@Getter
@AllArgsConstructor
public enum OrderSourceEnum {

    /**
     * 内部系统创建
     */
    Internal("内部系统创建"),
    /**
     * 第三方系统创建
     */
    External("第三方系统创建");

    private final String text;

}
