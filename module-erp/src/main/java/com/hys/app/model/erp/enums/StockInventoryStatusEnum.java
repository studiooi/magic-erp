package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存盘点单状态枚举
 *
 * @author dmy
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum StockInventoryStatusEnum {
    /**
     * 新创建（未提交）
     */
    NEW,
    /**
     * 已提交（待审核）
     */
    WAIT,
    /**
     * 审核通过
     */
    PASS,
    /**
     * 审核驳回
     */
    REJECT
}
