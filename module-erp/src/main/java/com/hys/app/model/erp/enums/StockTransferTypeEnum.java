package com.hys.app.model.erp.enums;

/**
 * 库存调拨单类型枚举
 *
 * @author 张崧
 * @since 2023-12-12
 */
public enum StockTransferTypeEnum {

    /**
     * 调出
     */
    Out("调出"),
    /**
     * 调入
     */
    In("调入");

    private String description;

    StockTransferTypeEnum(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }
}
