package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.LendForm;
import com.hys.app.model.erp.enums.LendFormStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商品借出单操作类
 *
 * @author dmy
 * 2023-12-06
 */
public class LendFormAllowable implements Serializable {

    private static final long serialVersionUID = -6533339419497949319L;

    @ApiModelProperty(name = "allow_edit", value = "是否允许编辑")
    private Boolean allowEdit;

    @ApiModelProperty(name = "allow_delete", value = "是否允许删除")
    private Boolean allowDelete;

    @ApiModelProperty(name = "allow_confirm", value = "是否允许确认")
    private Boolean allowConfirm;

    @ApiModelProperty(name = "allow_return", value = "是否允许归还")
    private Boolean allowReturn;

    public Boolean getAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(Boolean allowEdit) {
        this.allowEdit = allowEdit;
    }

    public Boolean getAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(Boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    public Boolean getAllowConfirm() {
        return allowConfirm;
    }

    public void setAllowConfirm(Boolean allowConfirm) {
        this.allowConfirm = allowConfirm;
    }

    public Boolean getAllowReturn() {
        return allowReturn;
    }

    public void setAllowReturn(Boolean allowReturn) {
        this.allowReturn = allowReturn;
    }

    @Override
    public String toString() {
        return "LendFormAllowable{" +
                "allowEdit=" + allowEdit +
                ", allowDelete=" + allowDelete +
                ", allowConfirm=" + allowConfirm +
                ", allowReturn=" + allowReturn +
                '}';
    }

    public LendFormAllowable(LendForm lendForm) {
        //获取借出单状态
        String status = lendForm.getStatus();
        //只有状态为新创建的借出单才可编辑
        this.setAllowEdit(LendFormStatusEnum.NEW.name().equals(status));
        //只有状态为新创建的借出单才可删除
        this.setAllowDelete(LendFormStatusEnum.NEW.name().equals(status));
        //只有状态为新创建的借出单才可确认
        this.setAllowConfirm(LendFormStatusEnum.NEW.name().equals(status));
        //只有状态为已确认的借出单才可归还
        this.setAllowReturn(LendFormStatusEnum.CONFIRMED.name().equals(status));
    }
}
