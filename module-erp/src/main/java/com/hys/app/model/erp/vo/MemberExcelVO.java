package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.enums.SexEnum;
import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 会员excel导出
 *
 * @author 张崧
 * 2024-01-23 09:28:16
 */
@Data
public class MemberExcelVO {

    @ExcelProperty("编号")
    private String sn;
    
    @ExcelProperty("姓名")
    private String name;
    
    @ExcelProperty("性别")
    private SexEnum sex;
    
    @ExcelProperty("手机号")
    private String mobile;
    
    @ExcelProperty("邮箱")
    private String email;
    
    @ExcelProperty("出生日期")
    private Long birthday;
    
    @ExcelProperty("备注")
    private String remark;
    
    @ExcelProperty("是否禁用")
    private Boolean disableFlag;
    
}

