package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 订单明细excel导出
 *
 * @author 张崧
 * 2024-01-24 16:39:38
 */
@Data
public class OrderItemExcelVO {

    @ExcelProperty("订单id")
    private Long orderId;
    
    @ExcelProperty("商品id")
    private Long goodsId;
    
    @ExcelProperty("产品id")
    private Long productId;
    
    @ExcelProperty("产品编号")
    private String productSn;
    
    @ExcelProperty("产品名称")
    private String productName;
    
    @ExcelProperty("产品规格")
    private String productSpecification;
    
    @ExcelProperty("产品单位")
    private String productUnit;
    
    @ExcelProperty("分类id")
    private Long categoryId;
    
    @ExcelProperty("分类名称")
    private String categoryName;
    
    @ExcelProperty("税率")
    private Double taxRate;
    
    @ExcelProperty("数量")
    private Integer num;
    
    @ExcelProperty("原始单价")
    private Double originPrice;
    
    @ExcelProperty("原始总价")
    private Double totalOriginPrice;
    
    @ExcelProperty("成本单价")
    private Double costPrice;
    
    @ExcelProperty("成本总价")
    private Double totalCostPrice;
    
    @ExcelProperty("税额")
    private Double taxPrice;
    
    @ExcelProperty("总税额")
    private Double totalTaxPrice;
    
    @ExcelProperty("销售单价")
    private Double price;
    
    @ExcelProperty("销售总价")
    private Double totalPrice;
    
    @ExcelProperty("优惠金额")
    private Double discountPrice;
    
    @ExcelProperty("备注")
    private String remark;
    
}

