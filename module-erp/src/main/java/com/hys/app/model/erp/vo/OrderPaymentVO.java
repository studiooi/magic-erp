package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单支付明细详情
 *
 * @author 张崧
 * 2024-01-24 17:00:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单支付明细详情")
public class OrderPaymentVO extends OrderPaymentDO {

}

