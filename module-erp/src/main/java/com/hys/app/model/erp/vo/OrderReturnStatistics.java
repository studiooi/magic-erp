package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.OrderReturnItemDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 订单退货统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class OrderReturnStatistics extends OrderReturnItemDO implements Serializable {

    private static final long serialVersionUID = -4667853079363496000L;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "distribution_name", value = "销售经理")
    private String distributionName;

    @ApiModelProperty(name = "sn", value = "退货单编号")
    private String sn;

    @ApiModelProperty(name = "order_sn", value = "要退货的订单编号")
    private String orderSn;

    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getDistributionName() {
        return distributionName;
    }

    public void setDistributionName(String distributionName) {
        this.distributionName = distributionName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Long returnTime) {
        this.returnTime = returnTime;
    }

    @Override
    public String toString() {
        return "OrderReturnStatistics{" +
                "deptName='" + deptName + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", distributionName='" + distributionName + '\'' +
                ", sn='" + sn + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", returnTime=" + returnTime +
                '}';
    }
}
