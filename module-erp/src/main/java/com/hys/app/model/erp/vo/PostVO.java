package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.PostDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 岗位详情
 *
 * @author 张崧
 * 2023-11-29 17:15:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PostVO extends PostDO {

}

