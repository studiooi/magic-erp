package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProcurementPlan;
import com.hys.app.model.erp.dos.ProcurementPlanProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 采购计划实体VO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementPlanVO extends ProcurementPlan implements Serializable {

    private static final long serialVersionUID = -1474473055795963268L;

    /**
     * 采购计划商品
     */
    @ApiModelProperty(name = "product_list", value = "采购计划商品")
    private List<ProcurementPlanProduct> productList;

    public List<ProcurementPlanProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ProcurementPlanProduct> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "ProcurementPlanVO{" +
                "productList=" + productList +
                '}';
    }
}
