package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockInventory;
import com.hys.app.model.erp.enums.StockInventoryStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存盘点单操作类
 *
 * @author dmy
 * 2023-12-06
 */
public class StockInventoryAllowable implements Serializable {

    private static final long serialVersionUID = 3969479363772175894L;

    @ApiModelProperty(name = "allow_edit", value = "是否允许编辑")
    private Boolean allowEdit;

    @ApiModelProperty(name = "allow_delete", value = "是否允许删除")
    private Boolean allowDelete;

    @ApiModelProperty(name = "allow_submit", value = "是否允许提交")
    private Boolean allowSubmit;

    @ApiModelProperty(name = "allow_audit", value = "是否允许审核")
    private Boolean allowAudit;

    @ApiModelProperty(name = "allow_cancel", value = "是否允许撤销")
    private Boolean allowCancel;

    public Boolean getAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(Boolean allowEdit) {
        this.allowEdit = allowEdit;
    }

    public Boolean getAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(Boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    public Boolean getAllowSubmit() {
        return allowSubmit;
    }

    public void setAllowSubmit(Boolean allowSubmit) {
        this.allowSubmit = allowSubmit;
    }

    public Boolean getAllowAudit() {
        return allowAudit;
    }

    public void setAllowAudit(Boolean allowAudit) {
        this.allowAudit = allowAudit;
    }

    public Boolean getAllowCancel() {
        return allowCancel;
    }

    public void setAllowCancel(Boolean allowCancel) {
        this.allowCancel = allowCancel;
    }

    @Override
    public String toString() {
        return "StockDamageReportAllowable{" +
                "allowEdit=" + allowEdit +
                ", allowDelete=" + allowDelete +
                ", allowSubmit=" + allowSubmit +
                ", allowAudit=" + allowAudit +
                ", allowCancel=" + allowCancel +
                '}';
    }

    public StockInventoryAllowable(StockInventory stockInventory) {
        //获取库存盘点单状态
        String status = stockInventory.getStatus();
        //只有状态为未提交（待审核）或审核驳回的库存盘点单才可编辑
        this.setAllowEdit(StockInventoryStatusEnum.NEW.name().equals(status) || StockInventoryStatusEnum.REJECT.name().equals(status));
        //只有状态为未提交（待审核）或审核驳回的库存盘点单才可删除
        this.setAllowDelete(StockInventoryStatusEnum.NEW.name().equals(status) || StockInventoryStatusEnum.REJECT.name().equals(status));
        //只有状态为未提交（待审核）的库存盘点单才可提交审核
        this.setAllowSubmit(StockInventoryStatusEnum.NEW.name().equals(status));
        //只有状态为待审核的库存盘点单才可进行审核
        this.setAllowAudit(StockInventoryStatusEnum.WAIT.name().equals(status));
        //只有状态为已提交（待审核）的库存盘点单才可进行撤销操作
        this.setAllowCancel(StockInventoryStatusEnum.WAIT.name().equals(status));
    }
}
