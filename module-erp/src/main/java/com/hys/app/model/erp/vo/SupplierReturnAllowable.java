package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierReturnDO;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.enums.SupplierReturnStatusEnum;
import com.hys.app.model.erp.enums.WarehouseEntryStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 供应商退货操作
 *
 * @author 张崧
 * @since 2023-12-14
 */
@Data
@NoArgsConstructor
public class SupplierReturnAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "submit", value = "是否允许提交")
    private Boolean submit;

    @ApiModelProperty(name = "withdraw", value = "是否允许撤回")
    private Boolean withdraw;

    @ApiModelProperty(name = "audit", value = "是否允许审核")
    private Boolean audit;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;

    public SupplierReturnAllowable(SupplierReturnDO supplierReturnDO) {
        SupplierReturnStatusEnum status = supplierReturnDO.getStatus();
        if(status == null){
            return;
        }

        this.setEdit(status == SupplierReturnStatusEnum.NotSubmit ||
                status == SupplierReturnStatusEnum.AuditReject);

        this.setSubmit(status == SupplierReturnStatusEnum.NotSubmit ||
                status == SupplierReturnStatusEnum.AuditReject);

        this.setDelete(status == SupplierReturnStatusEnum.NotSubmit ||
                status == SupplierReturnStatusEnum.AuditReject);

        this.setWithdraw(status == SupplierReturnStatusEnum.Submit);

        this.setAudit(status == SupplierReturnStatusEnum.Submit);
    }

}

