package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.enums.WarehouseEntryStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 入库单操作
 *
 * @author 张崧
 * @since 2023-12-06
 */
@Data
@NoArgsConstructor
public class WarehouseEntryAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "submit", value = "是否允许提交")
    private Boolean submit;

    @ApiModelProperty(name = "withdraw", value = "是否允许撤回")
    private Boolean withdraw;

    @ApiModelProperty(name = "audit", value = "是否允许审核")
    private Boolean audit;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;

    public WarehouseEntryAllowable(WarehouseEntryDO warehouseEntryDO) {
        WarehouseEntryStatusEnum status = warehouseEntryDO.getStatus();
        if(status == null){
            return;
        }

        this.setEdit(status == WarehouseEntryStatusEnum.NotSubmit ||
                status == WarehouseEntryStatusEnum.AuditReject);

        this.setSubmit(status == WarehouseEntryStatusEnum.NotSubmit ||
                status == WarehouseEntryStatusEnum.AuditReject);

        this.setDelete(status == WarehouseEntryStatusEnum.NotSubmit ||
                status == WarehouseEntryStatusEnum.AuditReject);

        this.setWithdraw(status == WarehouseEntryStatusEnum.Submit);

        this.setAudit(status == WarehouseEntryStatusEnum.Submit);
    }

}

