package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseOutDO;
import com.hys.app.model.erp.enums.WarehouseOutStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 出库单操作
 *
 * @author 张崧
 * @since 2024-01-04
 */
@Data
@NoArgsConstructor
public class WarehouseOutAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "audit", value = "是否允许审核")
    private Boolean audit;

    @ApiModelProperty(name = "ship", value = "是否允许发货")
    private Boolean ship;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;

    public WarehouseOutAllowable(WarehouseOutDO warehouseOutDO) {
        WarehouseOutStatusEnum status = warehouseOutDO.getStatus();
        if(status == null){
            return;
        }

        this.setEdit(status == WarehouseOutStatusEnum.WAIT_AUDIT ||
                status == WarehouseOutStatusEnum.AUDIT_PASS ||
                status == WarehouseOutStatusEnum.AUDIT_REJECT);

        this.setAudit(status == WarehouseOutStatusEnum.WAIT_AUDIT);

        this.setShip(status == WarehouseOutStatusEnum.WAIT_AUDIT ||
                status == WarehouseOutStatusEnum.AUDIT_PASS ||
                status == WarehouseOutStatusEnum.AUDIT_REJECT);

        this.setDelete(status == WarehouseOutStatusEnum.WAIT_AUDIT ||
                status == WarehouseOutStatusEnum.AUDIT_PASS ||
                status == WarehouseOutStatusEnum.AUDIT_REJECT);

    }

}

