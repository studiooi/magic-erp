package com.hys.app.model.shoptnt;

/**
 * ShopTnt接口API枚举
 *
 * @author 张崧
 * @since 2024-04-02
 */
public enum ShopTntApiEnum {
    /**
     * 接收消息
     */
    MessageReceive("/open/magic-erp/message-receive");

    private final String url;

    ShopTntApiEnum(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
