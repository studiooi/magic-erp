package com.hys.app.model.system.dto;

import com.hys.app.model.system.vo.DictDataBaseVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(value = "管理后台 - 字典数据创建 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class DictDataCreateDTO extends DictDataBaseVO {

}
