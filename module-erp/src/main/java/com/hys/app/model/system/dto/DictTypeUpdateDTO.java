package com.hys.app.model.system.dto;

import com.hys.app.model.system.vo.DictTypeBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "管理后台 - 字典类型更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class DictTypeUpdateDTO extends DictTypeBaseVO {

    @ApiModelProperty(value = "字典类型编号", example = "1024")
    @NotNull(message = "字典类型编号不能为空")
    private Long id;

}
