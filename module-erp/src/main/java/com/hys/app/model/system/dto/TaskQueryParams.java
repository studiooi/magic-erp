package com.hys.app.model.system.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 任务分页查询参数
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "任务分页查询参数")
public class TaskQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "name", value = "名称")
    private String name;
    
    @ApiModelProperty(name = "type", value = "类型")
    private String type;
    
    @ApiModelProperty(name = "sub_type", value = "子类型")
    private String subType;
    
    @ApiModelProperty(name = "params", value = "执行参数")
    private String params;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
    @ApiModelProperty(name = "result", value = "执行结果")
    private String result;
    
    @ApiModelProperty(name = "thread_id", value = "线程id")
    private String threadId;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

