package com.hys.app.model.system.enums;

/**
 * @author zjp
 * @version v7.0
 * @Description 消息模板编号枚举类
 * @ClassName MessageCodeEnum
 * @since v7.0 下午5:03 2018/7/5
 */
public enum MessageCodeEnum {

    EMAILCODESEND("邮箱发送验证码");

    private String description;

    MessageCodeEnum(String des) {
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
