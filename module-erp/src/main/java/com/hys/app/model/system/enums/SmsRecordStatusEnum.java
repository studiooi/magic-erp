package com.hys.app.model.system.enums;

/**
 * 短信发送状态枚举
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-02-05
 */
public enum SmsRecordStatusEnum {

    /**
     * 等待回执
     */
    WAIT_RECEIPT("等待回执"),

    /**
     * 用户接收成功
     */
    SUCCESS("用户接收成功"),

    /**
     * 用户接收失败
     */
    FAIL("用户接收失败");

    private String description;

    SmsRecordStatusEnum(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
    public String value(){
        return this.name();
    }
}
