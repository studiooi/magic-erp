package com.hys.app.model.system.enums;

/**
 * 短信业务编码枚举
 * @author zhangsong
 * @version v1.0
 * @since v1.0
 * 2021-02-05 17:17:30
 */
public enum SmsServiceCodeEnum {
    /**店铺新订单创建提醒*/
    SHOPORDERSNEW("店铺新订单创建提醒"),
    /**店铺订单支付提醒*/
    SHOPORDERSPAY("店铺订单支付提醒"),
    /**店铺订单收货提醒*/
    SHOPORDERSRECEIVE("店铺订单收货提醒"),
    /**店铺订单评价提醒*/
    SHOPORDERSEVALUATE("店铺订单评价提醒"),
    /**店铺订单取消提醒*/
    SHOPORDERSCANCEL("店铺订单取消提醒"),
    /**商品违规被禁售提醒（商品下架）*/
    SHOPGOODSVIOLATION("商品违规被禁售提醒（商品下架）"),
    /**商品审核失败提醒*/
    SHOPGOODSVERIFY("商品审核失败提醒"),
    /**售后服务退款提醒*/
    MEMBERREFUNDUPDATE("售后服务退款提醒"),
    /**订单发货提醒*/
    MEMBERORDERSSEND("订单发货提醒"),
    /**订单收货提醒*/
    MEMBERORDERSRECEIVE("订单收货提醒"),
    /**订单支付提醒*/
    MEMBERORDERSPAY("订单支付提醒"),
    /**订单得备货提醒*/
    MEMBERWAITSTOCK("订单得备货提醒"),
    /**订单备货完成提醒*/
    MEMBERSTOCKUP("订单备货完成提醒"),
    /**订单取消提醒*/
    MEMBERORDERSCANCEL("订单取消提醒"),
    /**商品下架消息提醒*/
    SHOPGOODSMARKETENABLE("商品下架消息提醒"),
    /**会员登录成功提醒*/
    MEMBERLOGINSUCCESS("会员登录成功提醒"),
    /**会员注册成功提醒*/
    MEMBERREGISTESUCCESS("会员注册成功提醒"),
    /**会员下单提醒*/
    MEMBERORDERCREATE("订单创建提醒"),
    /**店铺商品咨询提醒*/
    SHOPGOODSASK("店铺商品咨询提醒"),
    /**订单申请售后服务提醒*/
    SHOPAFTERSALE("订单申请售后服务提醒"),
    /**买家退还售后商品提醒*/
    SHOPAFTERSALEGOODSSHIP("买家退还售后商品提醒"),
    /**售后服务申请审核提醒*/
    MEMBERASAUDIT("售后服务申请审核提醒"),
    /**售后服务完成提醒*/
    MEMBERASCOMPLETED("售后服务完成提醒"),
    /**退货和换货售后服务审核通过提醒*/
    MEMBERASAUDITRETURNCHANGE("退货和换货售后服务审核通过提醒"),
    /**售后服务单关闭提醒*/
    MEMBERASCLOSED("售后服务单关闭提醒"),
    /**拼团订单自动取消提醒*/
    PINTUANORDERAUTOCANCEL("拼团订单自动取消提醒"),
    /**验证码-手机验证码验证*/
    MOBILECODESEND_VALIDATE_MOBILE("验证码-手机验证码验证"),
    /**验证码-手机绑定操作*/
    MOBILECODESEND_BIND_MOBILE("验证码-手机绑定操作"),
    /**验证码-添加店员*/
    MOBILECODESEND_ADD_CLERK("验证码-添加店员"),
    /**验证码-注册*/
    MOBILECODESEND_REGISTER("验证码-注册"),
    /**验证码-登录*/
    MOBILECODESEND_LOGIN("验证码-登录"),
    /**验证码-设置支付密码*/
    MOBILECODESEND_SET_PAY_PWD("验证码-设置支付密码"),
    /**验证码-找回密码*/
    MOBILECODESEND_FIND_PASSWORD("验证码-找回密码"),
    /**验证码-找回密码*/
    MEMBERASSHOPAUDIT("会员店铺审核提醒");

    private String description;

    SmsServiceCodeEnum(String des) {
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
