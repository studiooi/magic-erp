package com.hys.app.model.system.enums;

/**
 * 短信模板类型枚举
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
public enum SmsTemplateTypeEnum {

    /**
     * 验证码
     */
    CODE("验证码", 0, 1),

    /**
     * 短信通知
     */
    NOTIFICATION("短信通知", 1, 2),

    /**
     *  推广短信
     */
    PROMOTE("营销推广",2,3);
    private String description;
    /**
     * 阿里云短信接口对应的值
     */
    private Integer aliTypeCode;

    /**
     * 助通短信接口对应的值
     */
    private Integer ztTypeCode;


    SmsTemplateTypeEnum(String description, Integer aliTypeCode, Integer ztTypeCode) {
        this.description = description;
        this.aliTypeCode = aliTypeCode;
        this.ztTypeCode = ztTypeCode;
    }

    public String description() {
        return description;
    }

    public Integer aliTypeCode() {
        return aliTypeCode;
    }

    public Integer ztTypeCode() {
        return ztTypeCode;
    }

    public String value() {
        return this.name();
    }
}
