package com.hys.app.model.system.vo;

import com.hys.app.model.system.enums.DeptTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 部门 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class DeptBaseVO {

    @ApiModelProperty(value = "部门名称")
    @NotBlank(message = "部门名称不能为空")
    @Size(max = 30, message = "部门名称长度不能超过30个字符")
    private String name;

    @ApiModelProperty(value = "部门编号")
    @NotBlank(message = "部门编号不能为空")
    @Size(max = 30, message = "部门编号长度不能超过30个字符")
    private String sn;

    @ApiModelProperty(value = "父菜单 ID", example = "1024")
    private Long parentId;

    @ApiModelProperty(value = "部门类型", example = "INSTITUTION")
    private DeptTypeEnum type;

    @ApiModelProperty(value = "显示顺序不能为空", example = "1024")
    @NotNull(message = "显示顺序不能为空")
    private Integer sort;

    @ApiModelProperty(value = "部门说明不能为空", example = "1024")
    private String remark;

    @ApiModelProperty(value = "所在地区id")
    private List<Long> regionIds;

    @ApiModelProperty(value = "所在地区名称")
    private List<String> regionNames;

}
