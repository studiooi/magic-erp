package com.hys.app.model.system.vo;

import com.hys.app.model.system.dos.RoleDO;
import com.hys.app.model.system.enums.DataPermissionScopeEnum;
import com.hys.app.framework.util.JsonUtil;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 权限vo
 *
 * @author zh
 * @version v7.0
 * @date 18/6/25 上午10:59
 * @since v7.0
 */

public class RoleVO {

    @ApiModelProperty(hidden = true)
    private Long roleId;
    /**
     * 角色名称
     */
    @NotEmpty(message = "角色名称不能为空")
    @ApiModelProperty(name = "role_name", value = "角色名称", required = true)
    private String roleName;
    /**
     * 角色描述
     */
    @ApiModelProperty(name = "role_describe", value = "角色描述", required = true)
    private String roleDescribe;

    /**
     * 角色所拥有的菜单权限
     */
    @ApiModelProperty(name = "menus", value = "角色所拥有的菜单权限", required = true)
    private List<Menus> menus;

    /**
     * 数据权限的类型
     */
    @NotNull
    @ApiModelProperty(name = "data_scope", value = "数据权限的类型", required = false)
    private DataPermissionScopeEnum dataScope;
    /**
     * 数据权限指定的部门id集合（当类型为指定部门时，该字段有效）
     */
    @ApiModelProperty(name = "dept_ids", value = "部门id集合", required = false)
    private String deptIds;

    public RoleVO(RoleDO roleDO) {
        this.setRoleId(roleDO.getRoleId());
        this.setRoleName(roleDO.getRoleName());
        this.setRoleDescribe(roleDO.getRoleDescribe());
        this.setMenus(JsonUtil.jsonToList(roleDO.getAuthIds(), Menus.class));
        this.setDataScope(roleDO.getDataScope());
        this.setDeptIds(roleDO.getDeptIds());
    }

    public RoleVO() {

    }


    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<Menus> getMenus() {
        return menus;
    }

    public void setMenus(List<Menus> menus) {
        this.menus = menus;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleDescribe() {
        return roleDescribe;
    }

    public void setRoleDescribe(String roleDescribe) {
        this.roleDescribe = roleDescribe;
    }

    public DataPermissionScopeEnum getDataScope() {
        return dataScope;
    }

    public void setDataScope(DataPermissionScopeEnum dataScope) {
        this.dataScope = dataScope;
    }

    public String getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String deptIds) {
        this.deptIds = deptIds;
    }

    @Override
    public String toString() {
        return "RoleVO{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleDescribe='" + roleDescribe + '\'' +
                ", menus=" + menus +
                '}';
    }
}
