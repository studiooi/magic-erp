package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.ChangeFormAuditPassMessage;

/**
 * 换货单审核通过事件
 *
 * @author 张崧
 * @since 2024-01-09
 */
public interface ChangeFormAuditPassEvent {

    /**
     * 换货单单审核通过
     *
     * @param message
     */
    void onChangeFormAuditPass(ChangeFormAuditPassMessage message);

}
