package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.OrderReturnAuditPassMessage;

/**
 * 订单退货审核通过事件
 *
 * @author 张崧
 * @since 2024-01-09
 */
public interface OrderReturnAuditPassEvent {

    /**
     * 订单退货审核通过
     *
     * @param message
     */
    void onOrderReturnAuditPass(OrderReturnAuditPassMessage message);

}
