package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.StockDamageAuditPassMessage;

/**
 * 库存报损单审核通过事件
 *
 * @author 张崧
 * @since 2024-01-09
 */
public interface StockDamageAuditPassEvent {

    /**
     * 库存报损单审核通过
     *
     * @param message
     */
    void onStockDamageAuditPass(StockDamageAuditPassMessage message);

}
