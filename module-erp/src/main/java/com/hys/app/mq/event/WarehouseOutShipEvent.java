package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.WarehouseOutShipMessage;

/**
 * 出库单发货事件
 *
 * @author 张崧
 * @since 2024-01-09
 */
public interface WarehouseOutShipEvent {

    /**
     * 出库单发货
     *
     * @param message
     */
    void onWarehouseOutShip(WarehouseOutShipMessage message);

}
