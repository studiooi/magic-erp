package com.hys.app.mq.receiver;

import com.hys.app.mq.event.StockStatisticsEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.StockStatisticsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 库存统计 消息接收者
 *
 * @author dmy
 * 2023-12-22
 */
@Component
public class StockStatisticsReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<StockStatisticsEvent> events;

    /**
     * 处理库存统计消息
     *
     * @param message 库存统计消息
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.STOCK_STATISTICS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.STOCK_STATISTICS, type = ExchangeTypes.FANOUT)
    ))
    public void stockStatistics(StockStatisticsMessage message) {
        if (events != null) {
            for (StockStatisticsEvent event : events) {
                try {
                    System.out.println("-----------------------------------------------------------------------------库存统计消息接收者已接收到库存统计消息：" + message);
                    event.stockStatistics(message);
                } catch (Exception e) {
                    logger.error("处理库存统计消息出错", e);
                }
            }
        }
    }
}
