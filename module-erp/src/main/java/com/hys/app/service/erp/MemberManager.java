package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.MemberDO;
import com.hys.app.model.erp.vo.MemberVO;
import com.hys.app.model.erp.dto.MemberDTO;
import com.hys.app.model.erp.dto.MemberQueryParams;
import com.hys.app.framework.database.WebPage;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 会员业务层接口
 *
 * @author 张崧
 * 2024-01-23 09:28:16
 */
public interface MemberManager extends BaseService<MemberDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<MemberVO> list(MemberQueryParams queryParams);

    /**
     * 添加
     * @param memberDTO 新增数据
     */
    void add(MemberDTO memberDTO);

    /**
     * 编辑
     * @param memberDTO 更新数据
     */
    void edit(MemberDTO memberDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    MemberVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 导入
     * @param file
     */
    void importExcel(MultipartFile file);

    /**
     * 设置会员禁用
     * @param id
     * @param disableFlag
     */
    void setDisable(Long id, Boolean disableFlag);
}

