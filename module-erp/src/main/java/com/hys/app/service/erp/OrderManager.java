package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.dto.OrderQueryParams;
import com.hys.app.model.erp.dto.WarehouseOutShipDTO;
import com.hys.app.model.erp.vo.OrderVO;

import java.util.List;

/**
 * 订单业务层接口
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
public interface OrderManager extends BaseService<OrderDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<OrderVO> list(OrderQueryParams queryParams);

    /**
     * 添加
     *
     * @param orderDTO 新增数据
     * @return 订单编号
     */
    String add(OrderDTO orderDTO);

    /**
     * 编辑
     *
     * @param orderDTO 更新数据
     */
    void edit(OrderDTO orderDTO);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 详情数据
     */
    OrderVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 订单出库
     *
     * @param ids            订单id
     * @param warehouseOutId 出库单id
     */
    void warehouseOut(List<Long> ids, Long warehouseOutId);

    /**
     * 订单改为未出库
     *
     * @param warehouseOutIds 出库单id
     */
    void warehouseOutDelete(List<Long> warehouseOutIds);

    /**
     * 订单发货
     *
     * @param shipDTO 发货参数
     */
    void warehouseOutShip(WarehouseOutShipDTO shipDTO);

    /**
     * 提交
     *
     * @param ids 订单id
     */
    void submit(List<Long> ids);

    /**
     * 撤销提交
     *
     * @param ids 订单id
     */
    void withdraw(List<Long> ids);

    /**
     * 审核
     *
     * @param ids    订单id
     * @param isPass 是否通过
     * @param remark 审核备注
     */
    void audit(List<Long> ids, Boolean isPass, String remark);

    /**
     * 订单标记为已支付
     *
     * @param ids 订单id
     */
    void payment(List<Long> ids);
}

