package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.OrderReturnItemDO;
import com.hys.app.model.erp.vo.OrderReturnItemVO;
import com.hys.app.model.erp.dto.OrderReturnItemDTO;
import com.hys.app.model.erp.dto.OrderReturnItemQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 订单退货项业务层接口
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
public interface OrderReturnItemManager extends BaseService<OrderReturnItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<OrderReturnItemVO> list(OrderReturnItemQueryParams queryParams);

    /**
     * 添加
     * @param orderReturnItemDTO
     */
    void add(OrderReturnItemDTO orderReturnItemDTO);

    /**
     * 编辑
     * @param orderReturnItemDTO
     */
    void edit(OrderReturnItemDTO orderReturnItemDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    OrderReturnItemVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 根据退货单id删除
     * @param orderReturnIds
     */
    void deleteByOrderReturnId(List<Long> orderReturnIds);

    /**
     * 根据退货单id查询
     * @param orderReturnId
     * @return
     */
    List<OrderReturnItemDO> listByOrderReturnId(Long orderReturnId);
}

