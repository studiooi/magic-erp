package com.hys.app.service.erp;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.ProcurementContract;
import com.hys.app.model.erp.dto.ProcurementContractDTO;
import com.hys.app.model.erp.dto.ProcurementContractQueryParam;
import com.hys.app.model.erp.vo.ProcurementContractVO;

import java.util.List;

/**
 * 采购合同业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ProcurementContractManager extends IService<ProcurementContract> {

    /**
     * 查询采购合同分页列表数据
     *
     * @param params 查询参数
     * @return 分页数据
     */
    WebPage list(ProcurementContractQueryParam params);

    /**
     * 新增合同
     *
     * @param procurementContractDTO 合同信息
     */
    void add(ProcurementContractDTO procurementContractDTO);

    /**
     * 编辑合同
     *
     * @param id 主键ID
     * @param procurementContractDTO 合同信息
     */
    void edit(Long id, ProcurementContractDTO procurementContractDTO);

    /**
     * 删除合同
     *
     * @param ids 采购计划主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 获取合同详情
     *
     * @param id 合同ID
     * @return
     */
    ProcurementContractVO getDetail(Long id);

    /**
     * 获取所有合同信息
     *
     * @param status 合同状态
     * @return
     */
    List<ProcurementContract> listAll(String status);

    /**
     * 执行合同
     *
     * @param ids 采购计划主键ID集合
     */
    void execute(List<Long> ids);

    /**
     * 关闭合同
     *
     * @param ids 采购计划主键ID集合
     */
    void close(List<Long> ids);
}
