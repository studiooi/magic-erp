package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.ProductStockDO;
import com.hys.app.model.erp.dto.ProductStockQueryParams;
import com.hys.app.model.erp.enums.StockOperateEnum;
import com.hys.app.model.erp.vo.ProductStockVO;
import com.hys.app.model.erp.vo.ProductStockWarningVO;

import java.util.List;
import java.util.Map;

/**
 * 商品库存业务层接口
 *
 * @author 张崧
 * @since 2023-12-07 10:08:44
 */
public interface ProductStockManager extends BaseService<ProductStockDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<ProductStockVO> list(ProductStockQueryParams queryParams);

    /**
     * 初始化商品的库存
     *
     * @param productIds
     * @param goodsId
     */
    void initStockByProduct(List<Long> productIds, Long goodsId);

    /**
     * 初始化商品的库存
     *
     * @param warehouseId
     */
    void initStockByWarehouse(Long warehouseId);

    /**
     * 删除商品的库存
     *
     * @param productIds
     */
    void deleteStockByProduct(List<Long> productIds);

    /**
     * 删除商品的库存
     *
     * @param warehouseId
     */
    void deleteStockByWarehouse(Long warehouseId);

    /**
     * 查询商品库存预警分页列表数据
     *
     * @param pageNo      分页数
     * @param pageSize    每页数量
     * @param warehouseId 仓库ID
     * @param deptId      部门ID
     * @param name        商品名称
     * @return
     */
    WebPage<ProductStockWarningVO> listWarningStock(Long pageNo, Long pageSize, Long warehouseId, Long deptId, String name);

    /**
     * 查询多个产品在多个仓库的库存
     *
     * @param warehouseIds
     * @param productIds
     */
    List<ProductStockDO> listByWarehouseAndProduct(List<Long> warehouseIds, List<Long> productIds);

    /**
     * 查询产品库存数量
     *
     * @param warehouseIds
     * @param productIds
     */
    Map<Long, Integer> queryStockNum(List<Long> warehouseIds, List<Long> productIds);

    /**
     * 更新库存
     *
     * @param warehouseId
     * @param productId
     * @param operate
     * @param changeNum
     */
    void updateStock(Long warehouseId, Long productId, StockOperateEnum operate, int changeNum);
}

