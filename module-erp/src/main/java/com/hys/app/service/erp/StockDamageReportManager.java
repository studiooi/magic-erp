package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockDamageReportDTO;
import com.hys.app.model.erp.dto.StockDamageReportParam;
import com.hys.app.model.erp.dto.StockDamageReportStatisticsParam;
import com.hys.app.model.erp.vo.StockDamageReportVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 库存报损单业务接口
 *
 * @author dmy
 * 2023-12-05
 */
public interface StockDamageReportManager {

    /**
     * 查询库存报损单分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage list(StockDamageReportParam params);

    /**
     * 新增库存报损单
     *
     * @param stockDamageReportDTO 库存报损单信息
     */
    void add(StockDamageReportDTO stockDamageReportDTO);

    /**
     * 编辑库存报损单
     *
     * @param id 主键ID
     * @param stockDamageReportDTO 库存报损单信息
     */
    void edit(Long id, StockDamageReportDTO stockDamageReportDTO);

    /**
     * 删除库存报损单
     *
     * @param ids 库存报损单主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 获取库存报损单详情
     *
     * @param id 主键ID
     * @return
     */
    StockDamageReportVO getDetail(Long id);

    /**
     * 库存报损单提交审核
     *
     * @param ids 库存报损单主键ID集合
     */
    void submit(List<Long> ids);

    /**
     * 库存报损单撤销提交审核
     *
     * @param ids 库存报损单主键ID集合
     */
    void cancel(List<Long> ids);

    /**
     * 审核库存报损单
     *
     * @param ids 库存报损单ID集合
     * @param status 审核状态 PASS：审核通过，REJECT：审核驳回
     * @param rejectReason 驳回原因
     */
    void audit(List<Long> ids, String status, String rejectReason);

    /**
     * 查询库存报损单商品统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(StockDamageReportStatisticsParam params);

    /**
     * 导出库存报损单商品统计列表
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, StockDamageReportStatisticsParam params);
}
