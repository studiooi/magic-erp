package com.hys.app.service.erp;

import com.hys.app.model.erp.dos.StockInventoryProduct;
import com.hys.app.model.erp.dto.StockInventoryProductDTO;

import java.util.List;

/**
 * 库存盘点单产品业务接口
 *
 * @author dmy
 * 2023-12-05
 */
public interface StockInventoryProductManager {

    /**
     * 新增库存盘点单产品信息
     *
     * @param inventoryId 库存盘点单ID
     * @param productList 产品信息集合
     */
    void saveProduct(Long inventoryId, List<StockInventoryProductDTO> productList);

    /**
     * 删除库存盘点单产品信息
     *
     * @param inventoryIds 库存盘点单ID集合
     */
    void deleteProduct(List<Long> inventoryIds);

    /**
     * 根据库存盘点单ID获取库存盘点单产品信息集合
     * @param inventoryId 库存盘点单ID
     * @return
     */
    List<StockInventoryProduct> list(Long inventoryId);

}
