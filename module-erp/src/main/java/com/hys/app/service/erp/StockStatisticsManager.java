package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.StockStatistics;
import com.hys.app.model.erp.dto.StockStatisticsQueryParam;

import javax.servlet.http.HttpServletResponse;

/**
 * 库存统计业务接口
 *
 * @author dmy
 * 2023-12-05
 */
public interface StockStatisticsManager extends BaseService<StockStatistics> {

    /**
     * 查询库存统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage listStock(StockStatisticsQueryParam params);

    /**
     * 查询库存成本统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage listStockCost(StockStatisticsQueryParam params);

    /**
     * 根据批次ID获取库存统计信息
     *
     * @param batchId 批次ID
     * @return
     */
    StockStatistics getModel(Long batchId);

    /**
     * 修改产品库存预警数量
     *
     * @param productId 产品ID
     * @param stockWarningNum 库存预警数量
     */
    void updateStockWarningNum(Long productId, Integer stockWarningNum);

    /**
     * 导出库存统计列表数据
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, StockStatisticsQueryParam params);

    /**
     * 导出库存成本统计列表数据
     *
     * @param response
     * @param params 查询参数
     */
    void exportCost(HttpServletResponse response, StockStatisticsQueryParam params);
}
