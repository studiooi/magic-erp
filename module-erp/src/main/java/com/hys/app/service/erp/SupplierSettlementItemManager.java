package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.SupplierSettlementItemDO;
import com.hys.app.model.erp.vo.SupplierSettlementItemVO;
import com.hys.app.model.erp.dto.SupplierSettlementItemDTO;
import com.hys.app.model.erp.dto.SupplierSettlementItemQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 供应商结算单明细业务层接口
 *
 * @author 张崧
 * @since 2023-12-15 14:09:20
 */
public interface SupplierSettlementItemManager extends BaseService<SupplierSettlementItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<SupplierSettlementItemVO> list(SupplierSettlementItemQueryParams queryParams);

    /**
     * 添加
     * @param supplierSettlementItemDTO
     */
    void add(SupplierSettlementItemDTO supplierSettlementItemDTO);

    /**
     * 编辑
     * @param supplierSettlementItemDTO
     */
    void edit(SupplierSettlementItemDTO supplierSettlementItemDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    SupplierSettlementItemVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 根据结算单id删除
     * @param settlementId
     */
    void deleteBySettlementId(Long settlementId);

    /**
     * 根据结算单id查询
     * @param settlementId
     * @return
     */
    List<SupplierSettlementItemDO> listBySupplierSettlementId(Long settlementId);
}

