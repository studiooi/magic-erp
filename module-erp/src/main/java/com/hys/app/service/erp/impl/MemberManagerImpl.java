package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.MemberConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.MemberMapper;
import com.hys.app.model.erp.dos.MemberDO;
import com.hys.app.model.erp.dto.MemberDTO;
import com.hys.app.model.erp.dto.MemberQueryParams;
import com.hys.app.model.erp.vo.MemberVO;
import com.hys.app.model.system.dos.TaskDO;
import com.hys.app.model.system.enums.TaskSubTypeEnum;
import com.hys.app.model.system.enums.TaskTypeEnum;
import com.hys.app.service.erp.MemberManager;
import com.hys.app.service.system.task.TaskExecutor;
import com.hys.app.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 会员业务层实现
 *
 * @author 张崧
 * 2024-01-23 09:28:16
 */
@Service
public class MemberManagerImpl extends BaseServiceImpl<MemberMapper, MemberDO> implements MemberManager {

    @Autowired
    private MemberConverter converter;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private MemberImportManager memberImportManager;

    @Override
    public WebPage<MemberVO> list(MemberQueryParams queryParams) {
        WebPage<MemberDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(MemberDTO memberDTO) {
        // 校验参数
        checkParams(memberDTO);

        // 保存
        MemberDO memberDO = converter.convert(memberDTO);
        memberDO.setDisableFlag(false);
        save(memberDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(MemberDTO memberDTO) {
        // 校验参数
        checkParams(memberDTO);

        // 更新
        MemberDO memberDO = converter.convert(memberDTO);
        updateById(memberDO);
    }

    @Override
    public MemberVO getDetail(Long id) {
        MemberDO memberDO = getById(id);
        return converter.convert(memberDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void importExcel(MultipartFile file) {
        TaskDO task = new TaskDO();
        task.setType(TaskTypeEnum.Import);
        task.setSubType(TaskSubTypeEnum.MemberImport);
        task.setName("导入会员");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        taskExecutor.execute(task, () -> memberImportManager.doImportExcel(file, authentication), true);
    }

    @Override
    public void setDisable(Long id, Boolean disableFlag) {
        lambdaUpdate().set(MemberDO::getDisableFlag,disableFlag).eq(MemberDO::getId, id).update();
    }

    private void checkParams(MemberDTO memberDTO) {
        if (ValidateUtil.checkRepeat(baseMapper, MemberDO::getSn, memberDTO.getSn(), MemberDO::getId, memberDTO.getId())) {
            throw new ServiceException("会员编号已存在");
        }
        if (ValidateUtil.checkRepeat(baseMapper, MemberDO::getMobile, memberDTO.getMobile(), MemberDO::getId, memberDTO.getId())) {
            throw new ServiceException("手机号已存在");
        }
        if (ValidateUtil.checkRepeat(baseMapper, MemberDO::getEmail, memberDTO.getEmail(), MemberDO::getId, memberDTO.getId())) {
            throw new ServiceException("邮箱已存在");
        }
    }

}

