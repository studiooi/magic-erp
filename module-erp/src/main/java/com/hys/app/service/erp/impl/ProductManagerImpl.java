package com.hys.app.service.erp.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.hys.app.converter.erp.ProductConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.goods.ProductMapper;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.ProductDTO;
import com.hys.app.model.erp.dto.ProductQueryParams;
import com.hys.app.model.erp.dto.ProductSpec;
import com.hys.app.model.erp.vo.ProductVO;
import com.hys.app.service.erp.ProductManager;
import com.hys.app.service.erp.ProductStockManager;
import com.hys.app.service.goods.CategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.hys.app.framework.util.CollectionUtils.convertList;
import static com.hys.app.framework.util.CollectionUtils.convertMap;

/**
 * 产品业务层实现
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@Service
public class ProductManagerImpl extends BaseServiceImpl<ProductMapper, ProductDO> implements ProductManager {

    @Autowired
    private ProductConverter converter;

    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private ProductStockManager productStockManager;

    @Override
    public WebPage<ProductVO> list(ProductQueryParams queryParams) {
        WebPage<ProductDO> webPage = baseMapper.selectPage(queryParams);

        // 查询商品分类
        Map<Long, String> categoryNameMap = categoryManager.mapNameByIds(convertList(webPage.getData(), ProductDO::getCategoryId));
        // 查询库存信息
        List<Long> warehouseIds = queryParams.getWarehouseId() == null ? null : Collections.singletonList(queryParams.getWarehouseId());
        List<Long> productIds = convertList(webPage.getData(), ProductDO::getId);
        Map<Long, Integer> stockNumMap = productStockManager.queryStockNum(warehouseIds, productIds);

        return converter.combination(webPage, categoryNameMap, stockNumMap);
    }

    @Override
    public ProductVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    public void add(GoodsDO goodsDO, List<ProductDTO> productDTOList) {
        List<ProductDO> productList = converter.convertList(productDTOList, goodsDO);
        super.saveBatch(productList);

        // 初始化库存
        productStockManager.initStockByProduct(convertList(productList, ProductDO::getId), goodsDO.getId());
    }

    @Override
    public void edit(GoodsDO goodsDO, List<ProductDTO> productDTOList) {
        // 查询之前的数据，构建规格与SKU的映射关系
        Map<String, Long> oldProductMap = convertMap(listByGoodsId(goodsDO.getId()),
                productDO -> buildSpecKey(productDO.getSpecList()), ProductDO::getId);

        // 拆分三个集合，新插入的、需要更新的、需要删除的（oldProductMap）
        List<ProductDTO> insertDTOList = new ArrayList<>();
        List<ProductDTO> updateDTOList = new ArrayList<>();

        for (ProductDTO productDTO : productDTOList) {
            String buildSpecKey = buildSpecKey(productDTO.getSpecList());
            // 从旧sku中查找，未找到则新增，否则进行更新
            Long oldProductId = oldProductMap.remove(buildSpecKey);
            if (oldProductId == null) {
                insertDTOList.add(productDTO);
            } else {
                productDTO.setId(oldProductId);
                updateDTOList.add(productDTO);
            }
        }

        // 执行最终的批量操作
        if (CollUtil.isNotEmpty(insertDTOList)) {
            this.add(goodsDO, insertDTOList);
        }
        if (CollUtil.isNotEmpty(updateDTOList)) {
            List<ProductDO> updateList = converter.convertList(updateDTOList, goodsDO);
            super.updateBatchById(updateList);
        }
        if (CollUtil.isNotEmpty(oldProductMap)) {
            super.removeBatchByIds(oldProductMap.values());
        }
    }

    @Override
    public List<ProductDO> listByGoodsId(Long goodsId) {
        return lambdaQuery().eq(ProductDO::getGoodsId, goodsId).list();
    }

    @Override
    public void deleteByGoodsId(List<Long> goodsIds) {
        lambdaUpdate().in(ProductDO::getGoodsId, goodsIds).remove();
    }

    private String buildSpecKey(List<ProductSpec> specList) {
        if (CollUtil.isEmpty(specList)) {
            return StrUtil.EMPTY;
        }
        List<ProductSpec> specValueList = new ArrayList<>(specList);
        specValueList.sort(Comparator.comparing(ProductSpec::getSpecValueId));
        return specValueList.stream().map(specValueVO -> String.valueOf(specValueVO.getSpecValueId())).collect(Collectors.joining());
    }

}

