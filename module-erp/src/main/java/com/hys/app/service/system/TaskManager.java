package com.hys.app.service.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.system.dos.TaskDO;
import com.hys.app.model.system.dto.TaskQueryParams;
import com.hys.app.model.system.enums.TaskStatusEnum;
import com.hys.app.model.system.vo.TaskVO;

/**
 * 任务业务层接口
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
public interface TaskManager extends BaseService<TaskDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<TaskVO> list(TaskQueryParams queryParams);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 详情数据
     */
    TaskVO getDetail(Long id);

    /**
     * 查询执行中的任务数量
     *
     * @param task
     * @return
     */
    long countByRunning(TaskDO task);

    /**
     * 更新任务状态
     *
     * @param id
     * @param taskStatusEnum
     * @param result
     */
    void updateStatus(Long id, TaskStatusEnum taskStatusEnum, String result);

    /**
     * 更新线程id
     *
     * @param id
     * @param threadId
     */
    void updateThreadId(Long id, String threadId);
}

