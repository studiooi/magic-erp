package com.hys.app.service.system.factory;

import com.hys.app.service.base.plugin.sms.SmsPlatform;
import com.hys.app.model.system.vo.SmsPlatformVO;
import com.hys.app.service.system.SmsPlatformManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 短信发送插件工厂类
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-30
 */
@Component
public class SmsFactory {

    @Autowired
    private Map<String, SmsPlatform> smsPlatforms;

    @Autowired
    private SmsPlatformManager smsPlatformManager;

    /**
     * 获取当前开启的短信发送插件
     * @return
     */
    public SmsPlatform getSmsPlatform() {
        SmsPlatformVO platformVO = smsPlatformManager.getOpen();
        return this.smsPlatforms.get(platformVO.getBean());
    }

    /**
     * 根据插件id获取短信发送插件
     *
     * @param pluginId
     * @return
     */
    public SmsPlatform findByPluginId(String pluginId) {

        return this.smsPlatforms.get(pluginId);
    }

}
