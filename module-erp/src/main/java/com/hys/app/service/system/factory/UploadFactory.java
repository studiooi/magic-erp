package com.hys.app.service.system.factory;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hys.app.mapper.system.UploaderMapper;
import com.hys.app.model.system.vo.UploaderVO;
import com.hys.app.model.base.CachePrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hys.app.service.base.plugin.upload.Uploader;
import com.hys.app.model.system.dos.UploaderDO;
import com.hys.app.framework.cache.Cache;
import com.hys.app.framework.exception.ResourceNotFoundException;


/**
 * 存储方案VO
 *
 * @author zh
 * @version v1.0
 * @since v1.0
 * 2018年3月22日 上午9:39:08
 */
@Component
public class UploadFactory {


    @Autowired
    private List<Uploader> uploads;

    @Autowired
    private Cache cache;
    @Autowired
    private UploaderMapper uploaderMapper;


    /**
     * 获取存储方案对象
     *
     * @return 实例化的存储方案对象
     */
    public Uploader getUploader() {
        UploaderVO uploaderVo = getUploaderVO();
        //如果为空则要到库中读取
        if (uploaderVo == null) {
            //由数据库中查询存储方案
            QueryWrapper<UploaderDO> wrapper = createQueryWrapper();
            wrapper.eq("open",1);
            UploaderDO upload = uploaderMapper.selectOne(wrapper);

            if (upload == null) {
                throw new ResourceNotFoundException("未找到开启的存储方案");
            }
            uploaderVo = new UploaderVO();
            uploaderVo.setConfig(upload.getConfig());
            uploaderVo.setBean(upload.getBean());
            cache.put(getUploaderKey(), uploaderVo);
        }
        return this.findByBeanid(uploaderVo.getBean());
    }

    protected String getUploaderKey() {
        return CachePrefix.UPLOADER.getPrefix();
    }

    protected QueryWrapper<UploaderDO> createQueryWrapper() {
        return new QueryWrapper<>();
    }

    /**
     * 根据beanid获取出存储方案
     *
     * @param beanid
     * @return
     */
    private Uploader findByBeanid(String beanid) {
        for (Uploader iUploader : uploads) {
            if (iUploader.getPluginId().equals(beanid)) {
                return iUploader;
            }
        }
        //如果走到这里，说明找不到可用的存储方案
        throw new ResourceNotFoundException("未找到可用的文件存储方案");
    }


    public UploaderVO getUploaderVO() {
        return (UploaderVO) cache.get(getUploaderKey());
    }
}
