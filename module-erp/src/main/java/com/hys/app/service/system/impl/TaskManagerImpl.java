package com.hys.app.service.system.impl;

import com.hys.app.converter.system.TaskConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.system.TaskMapper;
import com.hys.app.model.system.dos.TaskDO;
import com.hys.app.model.system.dto.TaskQueryParams;
import com.hys.app.model.system.enums.TaskStatusEnum;
import com.hys.app.model.system.vo.TaskVO;
import com.hys.app.service.system.TaskManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 任务业务层实现
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
@Service
public class TaskManagerImpl extends BaseServiceImpl<TaskMapper, TaskDO> implements TaskManager {

    @Autowired
    private TaskConverter converter;

    @Override
    public WebPage<TaskVO> list(TaskQueryParams queryParams) {
        WebPage<TaskDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    public TaskVO getDetail(Long id) {
        TaskDO taskDO = getById(id);
        return converter.convert(taskDO);
    }

    @Override
    public long countByRunning(TaskDO task) {
        return lambdaQuery()
                .eq(TaskDO::getSubType, task.getSubType())
                .eq(TaskDO::getStatus, TaskStatusEnum.Running)
                .count();
    }

    @Override
    public void updateStatus(Long id, TaskStatusEnum taskStatusEnum, String result) {
        lambdaUpdate()
                .set(TaskDO::getStatus, taskStatusEnum)
                .set(TaskDO::getResult, result)
                .eq(TaskDO::getId, id)
                .update();
    }

    @Override
    public void updateThreadId(Long id, String threadId) {
        lambdaUpdate()
                .set(TaskDO::getThreadId, threadId)
                .eq(TaskDO::getId, id)
                .update();
    }
}

