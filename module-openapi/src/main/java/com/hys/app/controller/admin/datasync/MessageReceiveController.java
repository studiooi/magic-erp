package com.hys.app.controller.admin.datasync;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.datasync.dto.MessageReceiveQueryParams;
import com.hys.app.model.datasync.vo.MessageReceiveVO;
import com.hys.app.service.datasync.receive.common.MessageReceiveManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 消息接收API
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@RestController
@RequestMapping("/admin/datasync/messageReceive")
@Api(tags = "消息接收API")
@Validated
public class MessageReceiveController {

    @Autowired
    private MessageReceiveManager messageReceiveManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<MessageReceiveVO> list(MessageReceiveQueryParams queryParams) {
        return messageReceiveManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public MessageReceiveVO getDetail(@PathVariable Long id) {
        return messageReceiveManager.getDetail(id);
    }

    @ApiOperation(value = "手动执行一条消息")
    @GetMapping("/{id}/handle")
    public void handle(@PathVariable Long id) {
        messageReceiveManager.handle(id);
    }

}

