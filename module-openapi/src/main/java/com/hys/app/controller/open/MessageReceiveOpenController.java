package com.hys.app.controller.open;

import com.hys.app.model.datasync.dto.MessageReceiveDTO;
import com.hys.app.oauth2.LoginClientHolder;
import com.hys.app.service.datasync.receive.common.MessageReceiveManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 消息开放接口
 *
 * @author 张崧
 * @since 2024-04-01
 */
@RestController
@RequestMapping("/open/message-receive")
@Api(tags = "消息开放接口")
@Validated
public class MessageReceiveOpenController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageReceiveManager messageReceiveManager;

    /**
     * 接收成功标识
     */
    private static final String SUCCESS = "SUCCESS";

    @PostMapping
    @ApiOperation(value = "接收消息通知")
    public String receive(@RequestBody @Valid MessageReceiveDTO messageReceiveDTO) {

        logger.info("接收第三方消息推送\n 类型：{}\n 消息id：{}\n 消息时间：{}\n 消息内容：{}",
                messageReceiveDTO.getType(), messageReceiveDTO.getMsgId(), messageReceiveDTO.getProduceTime(), messageReceiveDTO.getContent());

        System.out.println(LoginClientHolder.getClientId());

        messageReceiveManager.receive(messageReceiveDTO);
        return SUCCESS;
    }

}
