package com.hys.app.controller.open.goods;

import com.hys.app.model.goods.vo.CategoryVO;
import com.hys.app.service.goods.CategoryManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品开放接口
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Api(tags = "商品分类开放接口")
@RestController
@RequestMapping("/open/goods-category")
@Validated
@Slf4j
public class CategoryOpenController {

    @Autowired
    private CategoryManager categoryManager;

    @ApiOperation(value = "查询所有商品分类")
    @PostMapping("/getAll")
    public List<CategoryVO> getAll() {
        return categoryManager.listAllChildren(0L);
    }

}
