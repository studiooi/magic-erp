package com.hys.app.controller.open.goods;

import com.hys.app.model.erp.vo.GoodsVO;
import com.hys.app.model.open.goods.dto.GetGoodsDetailDTO;
import com.hys.app.model.open.goods.dto.GetGoodsIdsDTO;
import com.hys.app.service.open.goods.GoodsOpenManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品开放接口
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Api(tags = "商品开放接口")
@RestController
@RequestMapping("/open/goods")
@Validated
@Slf4j
public class GoodsOpenController {

    @Autowired
    private GoodsOpenManager goodsOpenManager;

    @ApiOperation(value = "查询所有商品的id，返回空集合代表查询完成")
    @PostMapping("/getIds")
    public List<Long> getGoodsIds(@RequestBody @Valid GetGoodsIdsDTO getGoodsIdsDTO) {
        return goodsOpenManager.getGoodsIds(getGoodsIdsDTO);
    }

    @ApiOperation(value = "查询商品详情", response = GoodsVO.class)
    @PostMapping("/getDetail")
    public GoodsVO getGoodsDetail(@RequestBody @Valid GetGoodsDetailDTO getGoodsDetailDTO) {
        return goodsOpenManager.getDetail(getGoodsDetailDTO.getId());
    }

}
