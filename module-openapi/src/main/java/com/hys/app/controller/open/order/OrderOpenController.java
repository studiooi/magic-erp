package com.hys.app.controller.open.order;

import com.hys.app.model.open.order.dto.CreateOrderDTO;
import com.hys.app.service.open.order.OrderOpenManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 订单开放接口
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Api(tags = "订单开放接口")
@RestController
@RequestMapping("/open/order")
@Validated
@Slf4j
public class OrderOpenController {

    @Autowired
    private OrderOpenManager orderOpenManager;

    @ApiOperation(value = "创建订单")
    @PostMapping("/create")
    public String create(@RequestBody @Valid CreateOrderDTO createOrderDTO) {
        return orderOpenManager.create(createOrderDTO);
    }

}
