package com.hys.app.converter.open;

import com.hys.app.framework.util.CurrencyUtil;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.dto.OrderItemDTO;
import com.hys.app.model.open.order.dto.CreateOrderDTO;
import com.hys.app.model.open.order.dto.CreateOrderItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单 Converter
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OpenOrderConverter {

    OrderDTO convert(CreateOrderDTO createOrderDTO);
    default List<OrderItemDTO> convertItemList(List<CreateOrderItemDTO> itemList){
        return itemList.stream().map(createOrderItemDTO -> {
            OrderItemDTO orderItemDTO = new OrderItemDTO();
            orderItemDTO.setProductId(createOrderItemDTO.getProductId());
            orderItemDTO.setNum(createOrderItemDTO.getNum());
            orderItemDTO.setPrice(createOrderItemDTO.getPrice());
            orderItemDTO.setRemark(createOrderItemDTO.getRemark());
            orderItemDTO.setTaxRate(0D);
            orderItemDTO.setTaxPrice(0D);
            orderItemDTO.setDiscountPrice(0D);
            orderItemDTO.setTotalPrice(CurrencyUtil.mul(orderItemDTO.getPrice(), orderItemDTO.getNum()));
            orderItemDTO.setPayPrice(orderItemDTO.getTotalPrice());
            return orderItemDTO;
        }).collect(Collectors.toList());
    }
}

