package com.hys.app.job;

import com.hys.app.service.datasync.push.common.MessagePushManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时任务对推送失败的消息做补偿
 *
 * @author 张崧
 * @since 2023-12-19
 */
@JobHandler("messagePushJobHandler")
@Component
public class MessagePushJobHandler extends IJobHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessagePushManager messagePushManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        try {
            messagePushManager.scanFailedAndRetry();
            return ReturnT.SUCCESS;
        } catch (Exception e) {
            logger.error("消息推送定时任务执行失败", e);
            return ReturnT.FAIL;
        }
    }
}
