package com.hys.app.mapper.oauth2;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.oauth2.dos.OAuth2CodeDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * OAuth2 授权码 Mapper
 *
 * @author 张崧
 * @since 2024-02-20
 */
@Mapper
public interface OAuth2CodeMapper extends BaseMapperX<OAuth2CodeDO> {

    default OAuth2CodeDO selectByCode(String code) {
        return selectOne(OAuth2CodeDO::getCode, code);
    }

}
