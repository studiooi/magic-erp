package com.hys.app.model.datasync.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息接收状态
 *
 * @author 张崧
 * @since 2023-12-18
 */
@Getter
@AllArgsConstructor
public enum MessageReceiveStatusEnum {

    /**
     * 待处理
     */
    Wait,
    /**
     * 处理成功
     */
    Success,
    /**
     * 处理失败
     */
    Fail,

}
