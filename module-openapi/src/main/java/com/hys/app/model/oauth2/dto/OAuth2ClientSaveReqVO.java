package com.hys.app.model.oauth2.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 管理后台 - OAuth2 客户端创建/修改 Request VO
 *
 * @author 张崧
 * @since 2024-02-20
 */
@ApiModel(value = "管理后台 - OAuth2 客户端创建|修改 Request VO")
@Data
public class OAuth2ClientSaveReqVO {

    @ApiModelProperty(value = "编号", example = "1024")
    private Long id;

    @ApiModelProperty(value = "客户端编号")
    @NotBlank(message = "客户端编号不能为空")
    private String clientId;

    @ApiModelProperty(value = "客户端密钥")
    @NotBlank(message = "客户端密钥不能为空")
    private String secret;

    @ApiModelProperty(value = "应用名")
    @NotBlank(message = "应用名不能为空")
    private String name;

    @ApiModelProperty(value = "应用图标")
    @URL(message = "应用图标的地址不正确")
    private String logo;

    @ApiModelProperty(value = "应用描述")
    private String description;

    @ApiModelProperty(value = "状态，参见 CommonStatusEnum 枚举")
    @NotNull(message = "状态不能为空")
    private Integer status;

    @ApiModelProperty(value = "访问令牌的有效期")
    @NotNull(message = "访问令牌的有效期不能为空")
    private Integer accessTokenValiditySeconds;

    @ApiModelProperty(value = "刷新令牌的有效期")
    @NotNull(message = "刷新令牌的有效期不能为空")
    private Integer refreshTokenValiditySeconds;

    @ApiModelProperty(value = "授权类型，参见 OAuth2GrantTypeEnum 枚举")
    @NotNull(message = "授权类型不能为空")
    private List<String> authorizedGrantTypes;

    @ApiModelProperty(value = "可重定向的 URI 地址")
    private List<@NotBlank(message = "重定向的 URI 不能为空") @URL(message = "重定向的 URI 格式不正确") String> redirectUris;

    @ApiModelProperty(value = "授权范围", example = "user_info")
    private List<String> scopes;

    @ApiModelProperty(value = "自动通过的授权范围", example = "user_info")
    private List<String> autoApproveScopes;

}
