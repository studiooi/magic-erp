package com.hys.app.model.open.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 查询商品详情
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Data
@ApiModel(value = "查询商品详情")
public class GetGoodsDetailDTO {

    @ApiModelProperty(name = "id", value = "商品id")
    @NotNull(message = "id为空")
    private Long id;

}

