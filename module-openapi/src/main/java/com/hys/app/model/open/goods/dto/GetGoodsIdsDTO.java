package com.hys.app.model.open.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 查询商品id
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Data
@ApiModel(value = "查询商品id")
public class GetGoodsIdsDTO {

    @ApiModelProperty(name = "last_goods_id", value = "上次查询的最后一个商品id，首次查询传0")
    @NotNull(message = "last_goods_id为空")
    private Long lastGoodsId;

    @ApiModelProperty(name = "limit", value = "每次查询条数")
    @NotNull(message = "limit为空")
    @Min(value = 1, message = "limit最小为1")
    @Max(value = 500, message = "limit最大为500")
    private Integer limit;

}

