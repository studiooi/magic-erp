package com.hys.app.model.open.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 查询商品详情
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Data
@ApiModel(value = "查询商品详情")
public class GetGoodsStockDTO {

    @ApiModelProperty(name = "product_ids", value = "产品id集合")
    @NotEmpty(message = "productIds为空")
    private List<Long> productIds;

}

