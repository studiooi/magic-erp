package com.hys.app.model.open.order.dto;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.enums.OrderDeliveryType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 创建订单
 *
 * @author 张崧
 * @since 2024-04-08
 */
@Data
@ApiModel(value = "创建订单")
public class CreateOrderDTO {

    @ApiModelProperty(name = "third_sn", value = "第三方订单号")
    @NotBlank(message = "第三方订单号不能为空")
    private String thirdSn;

    @ApiModelProperty(name = "owner_id", value = "下单用户id")
    @NotBlank(message = "下单用户id不能为空")
    private String thirdOwnerId;

    @ApiModelProperty(name = "owner_name", value = "下单用户名称")
    @NotBlank(message = "下单用户名称不能为空")
    private String thirdOwnerName;

    @ApiModelProperty(name = "owner_mobile", value = "下单用户手机号")
    @NotBlank(message = "下单用户手机号不能为空")
    private String thirdOwnerMobile;

    @ApiModelProperty(name = "delivery_type", value = "配送方式")
    @NotNull(message = "配送方式不能为空")
    private OrderDeliveryType deliveryType;

    @ApiModelProperty(name = "ship_region_id", value = "物流配送地区id（3级或4级地区）")
    private Long shipRegionId;

    @ApiModelProperty(name = "ship_address", value = "物流配送详细地址")
    private String shipAddress;

    @ApiModelProperty(name = "store_id", value = "自提门店id")
    private Long storeId;

    @ApiModelProperty(name = "item_list", value = "订单项列表")
    @NotEmpty(message = "订单项不能为空")
    @Valid
    private List<CreateOrderItemDTO> itemList;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    // ============ 校验相关 ============

    @JsonIgnore
    @AssertFalse(message = "物流配送地址不能为空")
    public boolean isCheckShip() {
        return deliveryType == OrderDeliveryType.express && (shipRegionId == null || StringUtil.isEmpty(shipAddress));
    }

    @JsonIgnore
    @AssertFalse(message = "门店id不能为空")
    public boolean isCheckStore() {
        return deliveryType != null && deliveryType == OrderDeliveryType.self_pick && storeId == null;
    }

}

