package com.hys.app.model.open.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 创建订单-明细
 *
 * @author 张崧
 * 2024-04-08
 */
@Data
@ApiModel(value = "创建订单-明细")
public class CreateOrderItemDTO {

    @ApiModelProperty(name = "product_id", value = "产品id")
    @NotNull(message = "产品id不能为空")
    private Long productId;

    @ApiModelProperty(name = "num", value = "数量")
    @NotNull(message = "数量不能为空")
    @Min(value = 1, message = "数量必须大于0")
    private Integer num;

    @ApiModelProperty(name = "price", value = "销售价（单）")
    @NotNull(message = "销售价（单）不能为空")
    @Min(value = 0, message = "销售价（单）不能小于0")
    private Double price;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}

