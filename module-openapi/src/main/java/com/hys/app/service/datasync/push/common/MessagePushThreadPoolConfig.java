package com.hys.app.service.datasync.push.common;

import com.hys.app.model.base.ThreadPoolConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 消息推送线程池配置
 *
 * @author 张崧
 * @since 2023-12-18
 */
@Configuration
public class MessagePushThreadPoolConfig {

    @Bean(ThreadPoolConstant.MESSAGE_PUSH_POOL)
    public ThreadPoolTaskExecutor messagePushThreadPool() {
        int cpuNum = Runtime.getRuntime().availableProcessors();

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(cpuNum * 5);
        // 不使用非核心线程，全部进入队列中
        executor.setQueueCapacity(99999999);
        executor.setMaxPoolSize(cpuNum * 5);
        executor.setThreadNamePrefix("message-push-");
        executor.initialize();

        return executor;
    }

    @Bean(ThreadPoolConstant.COMMON_POOL)
    public ThreadPoolTaskExecutor commonThreadPool() {
        int cpuNum = Runtime.getRuntime().availableProcessors();

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(cpuNum * 2);
        executor.setQueueCapacity(100);
        executor.setMaxPoolSize(cpuNum * 4);
        executor.setThreadNamePrefix("common-");
        executor.initialize();

        return executor;
    }

}
