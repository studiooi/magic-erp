package com.hys.app.service.oauth2.impl;

import cn.hutool.core.util.IdUtil;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.mapper.oauth2.OAuth2CodeMapper;
import com.hys.app.model.oauth2.dos.OAuth2CodeDO;
import com.hys.app.service.oauth2.OAuth2CodeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * OAuth2.0 授权码 Service 实现类
 * 从功能上，和 Spring Security OAuth 的 JdbcAuthorizationCodeServices 的功能一致，提供授权码的操作
 *
 * @author 张崧
 * @since 2024-02-20
 */
@Service
@Validated
public class OAuth2CodeManagerImpl implements OAuth2CodeManager {

    /**
     * 授权码的过期时间，默认 5 分钟
     */
    private static final Integer TIMEOUT = 5 * 60;

    @Autowired
    private OAuth2CodeMapper oauth2CodeMapper;

    @Override
    public OAuth2CodeDO createAuthorizationCode(Long userId, Integer userType, String clientId,
                                                List<String> scopes, String redirectUri, String state) {
        OAuth2CodeDO codeDO = new OAuth2CodeDO().setCode(generateCode())
                .setUserId(userId).setUserType(userType)
                .setClientId(clientId).setScopes(scopes)
                .setExpiresTime(DateUtil.plusSeconds(TIMEOUT))
                .setRedirectUri(redirectUri).setState(state);
        oauth2CodeMapper.insert(codeDO);
        return codeDO;
    }

    @Override
    public OAuth2CodeDO consumeAuthorizationCode(String code) {
        OAuth2CodeDO codeDO = oauth2CodeMapper.selectByCode(code);
        if (codeDO == null) {
            throw new ServiceException("code 不存在");
        }
        if (DateUtil.isExpired(codeDO.getExpiresTime())) {
            throw new ServiceException("code 已过期");
        }
        oauth2CodeMapper.deleteById(codeDO.getId());
        return codeDO;
    }

    private static String generateCode() {
        return IdUtil.fastSimpleUUID();
    }

}
