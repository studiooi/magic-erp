package com.hys.app.service.open.goods;

import com.hys.app.model.erp.vo.GoodsVO;
import com.hys.app.model.open.goods.dto.GetGoodsIdsDTO;

import java.util.List;

/**
 * 商品对外接口业务层
 *
 * @author 张崧
 * @since 2024-04-08
 */
public interface GoodsOpenManager {

    /**
     * 查询商品id
     *
     * @param getGoodsIdsDTO
     * @return
     */
    List<Long> getGoodsIds(GetGoodsIdsDTO getGoodsIdsDTO);

    /**
     * 查询商品详情
     *
     * @param id
     * @return
     */
    GoodsVO getDetail(Long id);
}

