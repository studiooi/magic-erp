package com.hys.app.service.open.order;

import com.hys.app.model.open.order.dto.CreateOrderDTO;

/**
 * 订单对外接口业务层
 *
 * @author 张崧
 * @since 2024-04-08
 */
public interface OrderOpenManager {

    /**
     * 创建订单
     *
     * @param createOrderDTO
     * @return
     */
    String create(CreateOrderDTO createOrderDTO);
}

